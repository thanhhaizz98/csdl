-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th6 16, 2021 lúc 04:04 PM
-- Phiên bản máy phục vụ: 10.3.29-MariaDB-cll-lve
-- Phiên bản PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `royalsan_rsn_web`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `score` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(320) NOT NULL,
  `address` text NOT NULL,
  `dob` date NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `isActivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `username`, `password`, `score`, `name`, `phone`, `email`, `address`, `dob`, `gender`, `isActivated`) VALUES
(21, '0348371266', 'huong123', 0, 'Vũ Thị Hương', '0348371266', 'huong@gmail.com', 'Hải Phòng', '2021-06-10', 1, 1),
(22, '0898288422', 'hoang111', 0, 'Đặng Việt Hoàng', '0898288422', 'Hoang@gmail.com', 'Hải Phòng', '2021-06-02', 1, 1),
(23, '0358528265', 'Abc@123', 0, 'Huong Vu', '0358528265', 'thanhhaizz98@gmail.com', 'Hà Nam', '2021-06-07', 1, 1),
(24, '0348371268', '1', 0, 'Vũ Thị Hương', '0348371268', 'thanhhaizz@gmail.com', 'Hà Nam', '2021-05-31', 1, 1),
(25, '0373476978', 'Abc@123', 0, 'pham khac hoang', '0373476978', 'pham29700@gmail.com', 'Thủy Nguyên', '2021-06-07', 1, 1),
(26, '0898288423', 'Hoang123', 0, 'Phạm Hoàng', '0898288423', 'hoang@111.com', 'Thủy Nguyên', '2021-06-08', 1, 1),
(27, '0964976898', '1234nam', 0, 'Phạm Nguyễn Thanh Hải', '0964976898', '', '', '2021-06-09', 0, 1),
(28, '0987654321', 'maiyeu', 0, 'Thanh Hải', '0987654321', 'thanhhai@gmail.com', 'Kom Tom', '2021-06-08', 1, 1),
(29, '0988777666', 'hoang123', 0, 'Đặng Hoàng ', '0988777666', '', '', '2021-06-10', 0, 1),
(30, '0976846464', '1234', 0, 'Thanh Hải ', '0976846464', 'thanhhaizz@gmail.com', 'Hà Nam', '2021-06-09', 0, 1),
(35, '0123456789', 'okok', 0, 'Chào em', '0123456789', 'limollnguyen.vn@gmail.com', 'Tây Nguyên', '2021-06-09', 1, 1),
(36, '0888888888', 'Hoang@123', 0, 'Hương Hoàng', '0888888888', '', '', '2021-06-13', 0, 1),
(37, '0898288111', 'hoang123', 0, 'Hoang Dang', '0898288111', '', '', '2021-06-13', 0, 1),
(38, '0348371265', '123456', 0, 'Vu Huong', '0348371265', '', '', '2021-06-13', 0, 1),
(39, '0964976888', '12345', 0, 'Chào em', '0964976888', 'thanhhaizz@gmail.com', 'Tây Ninh', '2021-06-13', 0, 1),
(40, '0898288421', 'hoang123', 0, 'Dang Dang Hoang', '0898288421', '', '', '2021-06-14', 0, 1),
(41, '0898288424', '123456', 0, 'Hoang Hoang', '0898288424', '', '', '2021-06-14', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `inventoryDelivery_voucher`
--

CREATE TABLE `inventoryDelivery_voucher` (
  `id` int(11) NOT NULL,
  `warehouseFrom` int(11) NOT NULL,
  `warehouseTo` int(11) NOT NULL,
  `totalQuantity` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `inventoryDelivery_voucher`
--

INSERT INTO `inventoryDelivery_voucher` (`id`, `warehouseFrom`, `warehouseTo`, `totalQuantity`, `dateCreated`) VALUES
(21, 6, 7, 5, '2021-06-14 11:23:21'),
(22, 5, 8, 7, '2021-06-14 12:19:12'),
(23, 6, 7, 7, '2021-06-14 12:20:27'),
(24, 8, 7, 8, '2021-06-14 12:22:09'),
(25, 6, 7, 5, '2021-06-14 12:31:26'),
(26, 6, 7, 2, '2021-06-14 12:46:17'),
(27, 7, 6, 1, '2021-06-14 12:52:28'),
(28, 5, 7, 1, '2021-06-14 14:28:06'),
(29, 6, 5, 1, '2021-06-14 15:41:30'),
(30, 6, 5, 1, '2021-06-14 15:42:34'),
(31, 6, 5, 1, '2021-06-14 15:46:25'),
(32, 6, 5, 1, '2021-06-14 15:49:15'),
(33, 6, 5, 1, '2021-06-14 15:56:44'),
(34, 6, 5, 1, '2021-06-14 15:57:28'),
(35, 6, 5, 1, '2021-06-14 15:58:19'),
(36, 6, 5, 1, '2021-06-14 15:59:45'),
(37, 6, 5, 1, '2021-06-14 16:01:42'),
(38, 6, 5, 1, '2021-06-14 16:03:20'),
(39, 6, 5, 1, '2021-06-14 16:04:43'),
(40, 6, 5, 1, '2021-06-14 16:05:17'),
(41, 5, 6, 1, '2021-06-14 16:07:25'),
(42, 6, 5, 1, '2021-06-14 16:11:16'),
(43, 6, 5, 1, '2021-06-14 16:16:13'),
(44, 6, 5, 1, '2021-06-14 16:27:14'),
(45, 5, 6, 1, '2021-06-14 17:06:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `inventoryDelivery_voucher_detail`
--

CREATE TABLE `inventoryDelivery_voucher_detail` (
  `id` int(11) NOT NULL,
  `inventoryDeliveryVoucherId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `inventoryDelivery_voucher_detail`
--

INSERT INTO `inventoryDelivery_voucher_detail` (`id`, `inventoryDeliveryVoucherId`, `productId`, `quantity`) VALUES
(1, 21, 30, 1),
(2, 21, 36, 1),
(3, 21, 26, 1),
(4, 21, 10, 1),
(5, 21, 20, 1),
(6, 22, 1, 1),
(7, 22, 11, 1),
(8, 22, 16, 1),
(9, 22, 12, 1),
(10, 22, 13, 1),
(11, 22, 33, 1),
(12, 22, 42, 1),
(13, 23, 1, 1),
(14, 23, 11, 1),
(15, 23, 16, 1),
(16, 23, 12, 1),
(17, 23, 13, 1),
(18, 23, 33, 1),
(19, 23, 42, 1),
(20, 24, 1, 1),
(21, 24, 11, 1),
(22, 24, 16, 1),
(23, 24, 12, 1),
(24, 24, 13, 1),
(25, 24, 33, 1),
(26, 24, 42, 1),
(27, 24, 41, 1),
(28, 25, 10, 1),
(29, 25, 26, 1),
(30, 25, 36, 1),
(31, 25, 20, 1),
(32, 25, 30, 1),
(33, 26, 10, 2),
(34, 26, 20, 2),
(35, 27, 20, 6),
(36, 28, 1, 4),
(37, 29, 10, 10),
(38, 30, 10, 10),
(39, 31, 10, 10),
(40, 32, 10, 10),
(41, 33, 30, 30),
(42, 34, 30, 30),
(43, 35, 30, 30),
(44, 36, 30, 30),
(45, 37, 30, 30),
(46, 38, 30, 30),
(47, 39, 30, 30),
(48, 40, 30, 30),
(49, 41, 30, 5),
(50, 42, 10, 8),
(51, 43, 10, 5),
(52, 44, 10, 5),
(53, 45, 10, 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `inwardSlip_voucher`
--

CREATE TABLE `inwardSlip_voucher` (
  `id` int(11) NOT NULL,
  `invoiceNumber` varchar(200) CHARACTER SET utf8 NOT NULL,
  `statusPay` tinyint(1) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `vendorId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `voucherDate` datetime NOT NULL,
  `accountingDate` datetime NOT NULL,
  `totalQuantity` int(11) NOT NULL,
  `totalTaxAmount` decimal(18,2) NOT NULL,
  `totalAmount` decimal(18,2) NOT NULL,
  `totalPayment` decimal(18,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `inwardSlip_voucher`
--

INSERT INTO `inwardSlip_voucher` (`id`, `invoiceNumber`, `statusPay`, `description`, `vendorId`, `employeeId`, `dateCreated`, `voucherDate`, `accountingDate`, `totalQuantity`, `totalTaxAmount`, `totalAmount`, `totalPayment`) VALUES
(15, 'HD0000012', 0, 'Chi trả tiền hàng cho NCC', 3, 7, '2021-05-29 02:15:00', '2021-05-29 02:15:23', '2021-05-29 02:15:23', 8, 150.00, 12000000.00, 12000150.00),
(19, '34', 0, 'Chi trả tiền hàng cho NCC', 3, 19, '2021-05-29 23:06:23', '2021-05-29 23:06:23', '2021-05-29 23:06:23', 4, 16000.00, 6000000.00, 6016000.00),
(20, '12', 0, 'Chi trả tiền hàng cho NCC', 5, 9, '2021-05-29 23:12:23', '2021-05-29 23:12:25', '2021-05-29 23:12:25', 3, 0.00, 10500000.00, 10500000.00),
(21, '120', 0, 'Chi trả tiền hàng cho NCC', 3, 7, '2021-05-29 23:13:32', '2021-05-29 23:13:34', '2021-05-29 23:13:34', 4, 15.00, 80000.00, 80015.00),
(22, '96', 1, 'Chi trả tiền hàng cho NCC', 5, 16, '2021-05-29 23:14:06', '2021-05-29 23:14:07', '2021-05-29 23:14:07', 3, 15.00, 21000000.00, 21000015.00),
(23, 'HD0000011', 0, 'Chi trả tiền hàng cho NCC', 3, 13, '2021-06-02 00:30:38', '2021-06-02 00:30:58', '2021-06-02 00:30:58', 3, 137000.00, 1146111.00, 1283111.00),
(26, 'HD0000021', 0, 'Chi trả tiền hàng cho NCC', 5, 7, '2021-06-02 13:47:52', '2021-06-02 13:48:13', '2021-06-02 13:48:13', 2, 0.00, 10500000.00, 10500000.00),
(27, '1233', 0, 'Chi trả tiền hàng cho NCC', 1, 6, '2021-06-09 02:09:00', '2021-06-09 02:08:59', '2021-07-10 02:08:07', 10, 20000.00, 20000000.00, 20020000.00),
(28, 'hd0001231', 0, 'Chi trả tiền hàng cho NCC, nếu đại diện nhà cung cấp thì phải có giấy tờ xác minh', 5, 13, '2021-06-09 03:25:26', '2021-06-09 03:25:25', '2023-06-14 03:24:18', 116, 0.00, 529500000.00, 529500000.00),
(29, 'hd000012', 1, 'Chi trả tiền hàng cho NCC', 8, 7, '2021-06-09 03:34:29', '2021-03-06 00:00:00', '2022-06-09 00:00:00', 20, 0.00, 100000000.00, 100000000.00),
(30, 'hd000013', 0, 'Chi trả tiền hàng cho NCC', 3, 6, '2021-06-09 03:38:39', '2021-08-09 00:00:00', '2022-06-08 03:37:14', 21, 0.00, 71500000.00, 71500000.00),
(31, 'hd000014', 1, 'Chi trả tiền hàng cho NCC', 6, 16, '2021-06-09 03:41:26', '2021-05-30 03:40:35', '2022-06-28 00:00:00', 8, 0.00, 4400000.00, 4400000.00),
(32, 'hd0001231', 1, 'Chi trả tiền hàng cho NCC', 6, 8, '2021-06-09 03:43:25', '2021-06-09 03:43:24', '2023-06-09 00:00:00', 44, 0.00, 28800000.00, 28800000.00),
(33, 'hd0000130', 1, 'Chi trả tiền hàng cho NCC', 1, 22, '2021-06-09 03:48:49', '1999-09-05 03:46:10', '2021-06-09 03:48:48', 68, 0.00, 33240000.00, 33240000.00),
(34, 'hd00001312', 1, 'Chi trả tiền hàng cho NCC', 8, 9, '2021-06-09 03:52:24', '2021-06-09 03:52:23', '2021-06-09 03:52:23', 5, 0.00, 13350000.00, 13350000.00),
(35, 'hd000014', 1, 'Chi trả tiền hàng cho NCC', 6, 13, '2021-06-09 03:53:56', '2021-06-09 03:53:55', '2021-06-09 03:53:55', 19, 0.00, 11700000.00, 11700000.00),
(36, 'HD1122', 0, 'Chi trả tiền hàng cho NCC', 3, 16, '2021-06-09 08:32:07', '2021-06-09 08:32:06', '2021-06-09 08:32:06', 3, 10000.00, 5750000.00, 5760000.00),
(37, '1112', 0, 'Chi trả tiền hàng cho NCC', 6, 13, '2021-06-09 08:33:51', '2021-06-09 08:33:49', '2021-06-09 08:33:49', 25, 60000.00, 25500000.00, 25560000.00),
(38, '1233', 0, 'Chi trả tiền hàng cho NCC', 5, 6, '2021-06-14 15:54:43', '2021-06-14 15:54:42', '2021-06-14 15:54:42', 5, 5000.00, 4250000.00, 4255000.00),
(39, 'HD00000', 0, 'Chi trả tiền hàng cho NCC', 3, 8, '2021-06-14 17:02:26', '2021-06-14 17:02:46', '2021-06-14 17:02:46', 4, 10000.00, 6000000.00, 6010000.00);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(320) NOT NULL,
  `paymentMethod` tinyint(4) NOT NULL,
  `shippingMethod` tinyint(4) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `note` text NOT NULL,
  `statusPay` tinyint(1) NOT NULL,
  `totalAmount` decimal(18,2) NOT NULL,
  `isActivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customerId`, `name`, `phone`, `address`, `email`, `paymentMethod`, `shippingMethod`, `dateCreated`, `note`, `statusPay`, `totalAmount`, `isActivated`) VALUES
(21, 22, 'Đặng Việt Hoàng', '0898288423', 'Hải Phòng', 'hoangdv.hp99@gmail.com', 0, 0, '2021-06-06 13:40:06', '', 0, 5000000.00, 1),
(22, 22, 'Đặng Việt Hoàng', '0898288422', 'Hà Nội', '', 0, 0, '2021-06-06 14:47:55', 'hello', 0, 5000000.00, 0),
(23, 24, 'Vũ Thị Hương', '0348371268', 'Hà Nam', 'hai69475@st.vimaru.edu.vn', 1, 1, '2021-06-08 03:38:33', 'Yến rất ngon', 0, 35350000.00, 0),
(24, 24, 'Vũ Thị Hương', '0348371268', 'Tiên Lãng - Hải Phòngg', 'thanhhaizz@gmail.com', 1, 1, '2021-06-08 03:46:22', 'Yến rất ngon', 0, 27150000.00, 1),
(25, 22, 'Đặng Việt Hoàng', '0898288423', 'Hải Phòng', '', 0, 0, '2021-06-08 03:58:09', '', 0, 39500000.00, 1),
(26, 23, 'Huong Vu', '0358528265', 'Thuy Nguyen', 'pham29700@gmail.com', 1, 1, '2021-06-08 10:40:44', 'nothing', 0, 3000000.00, 2),
(27, 21, 'Vũ Thị Hương', '0348371266', '1a Đằng hải, hải an, Hải Phòng', 'huong@gmail.com', 1, 1, '2021-06-09 00:39:59', '', 0, 7000000.00, 1),
(28, 26, 'Phạm Hoàng', '0898288423', 'Thủy Nguyên', 'hoang@111.com', 1, 1, '2021-06-09 08:19:22', '', 0, 32630000.00, 1),
(29, 26, 'Phạm Hoàng', '0898288423', 'Thủy Nguyên', 'hoang@111.com', 0, 0, '2021-06-09 08:23:22', 'Đơn hàng bị hủy bởi khách hàng lúc15:12:36 - 10/06/2021', 0, 3000000.00, 3),
(30, 25, 'pham khac hoang', '0373476978', '11 dong khe', 'pham29700@gmail.com', 2, 1, '2021-06-09 08:30:37', '', 0, 10750000.00, 0),
(31, 24, 'Vũ Thị Hương', '0348371268', 'Hà Nam', 'hai69475@st.vimaru.edu.vn', 2, 1, '2021-06-09 23:47:42', 'yến ngon', 0, 6000000.00, 1),
(32, 24, 'Vũ Thị Hương', '0348371268', 'Hà Nam', 'thanhhaizz@gmail.com', 2, 1, '2021-06-09 23:51:07', '', 0, 1000000.00, 0),
(33, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 1, 1, '2021-06-10 13:25:40', 'yêu nhé', 0, 36930000.00, 1),
(34, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 0, 0, '2021-06-10 13:39:17', '', 0, 4900000.00, 1),
(35, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 0, 0, '2021-06-10 13:40:14', 'Đơn hàng bị hủy bởi khách hàng lúc 16:27:48 - 10/06/2021', 0, 3900000.00, 3),
(36, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 0, 0, '2021-06-10 13:41:24', 'Đơn hàng bị hủy bởi khách hàng lúc 18:11:35 - 13/06/2021', 0, 1000000.00, 3),
(37, 26, 'Phạm Hoàng', '0898288423', 'Thủy Nguyên', 'hoang@111.com', 0, 0, '2021-06-10 14:45:27', 'Đơn hàng bị hủy bởi khách hàng lúc15:08:49 - 10/06/2021', 0, 7500000.00, 3),
(38, 26, 'Phạm Hoàng', '0898288423', 'Thủy Nguyên', 'hoang@111.com', 0, 0, '2021-06-10 15:18:55', '', 0, 4000000.00, 1),
(39, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 1, 1, '2021-06-10 16:19:32', 'Yêu', 0, 16180000.00, 1),
(40, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 0, 0, '2021-06-10 16:20:48', 'yêu', 0, 15650000.00, 3),
(41, 28, 'Thanh Hải', '0987654321', 'Kom Tom', 'thanhhai@gmail.com', 0, 0, '2021-06-10 16:32:07', '', 0, 15650000.00, 2),
(42, 36, 'Hương Hoàng', '0888888888', 'Thủy Nguyên - Hải Phòng', 'hoangdv.hp99@gmail.com', 1, 1, '2021-06-13 19:30:34', '', 0, 11430000.00, 1),
(43, 39, 'Chào em', '0964976888', 'Tây Ninh', 'thanhhaizz@gmail.com', 1, 1, '2021-06-14 12:55:11', 'mãi yêu', 0, 7930000.00, 1),
(44, 21, 'Vũ Thị Hương', '0348371266', 'Hải Phòng', 'huong@gmail.com', 0, 0, '2021-06-14 13:08:47', '', 0, 1000000.00, 0),
(45, 25, 'pham khac hoang', '0373476978', 'Thủy Nguyên', 'pham29700@gmail.com', 2, 1, '2021-06-14 15:20:14', 'Đơn hàng bị hủy bởi khách hàng lúc 15:29:59 - 14/06/2021', 0, 3030000.00, 1),
(46, 25, 'pham khac hoang', '0373476978', 'Thủy Nguyên', 'pham29700@gmail.com', 0, 0, '2021-06-14 15:34:20', '', 0, 1000000.00, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `orderId`, `productId`, `quantity`) VALUES
(14, 21, 13, 1),
(15, 22, 13, 1),
(16, 23, 10, 1),
(17, 23, 11, 1),
(18, 23, 13, 1),
(19, 23, 20, 1),
(20, 23, 24, 1),
(21, 23, 25, 1),
(22, 23, 30, 1),
(23, 23, 32, 1),
(24, 23, 27, 1),
(25, 23, 16, 1),
(26, 24, 1, 1),
(27, 24, 11, 1),
(28, 24, 13, 1),
(29, 24, 20, 1),
(30, 24, 25, 1),
(31, 24, 24, 1),
(32, 24, 17, 1),
(33, 24, 27, 1),
(34, 24, 23, 1),
(35, 24, 29, 1),
(36, 25, 11, 2),
(37, 25, 13, 4),
(38, 25, 20, 1),
(39, 25, 1, 5),
(40, 25, 24, 1),
(41, 25, 25, 1),
(42, 26, 10, 1),
(43, 26, 11, 1),
(44, 27, 28, 6),
(45, 27, 4, 1),
(46, 27, 10, 1),
(47, 28, 1, 4),
(48, 28, 10, 2),
(49, 28, 12, 4),
(50, 29, 11, 1),
(51, 29, 10, 1),
(52, 30, 4, 1),
(53, 30, 10, 1),
(54, 30, 16, 1),
(55, 30, 13, 1),
(56, 30, 17, 1),
(57, 31, 10, 1),
(58, 31, 11, 3),
(59, 32, 4, 1),
(60, 33, 1, 1),
(61, 33, 4, 1),
(62, 33, 16, 1),
(63, 33, 32, 1),
(64, 33, 11, 2),
(65, 33, 10, 2),
(66, 33, 25, 1),
(67, 33, 24, 1),
(68, 33, 29, 1),
(69, 34, 1, 1),
(70, 34, 4, 1),
(71, 35, 1, 1),
(72, 36, 13, 2),
(73, 37, 4, 3),
(74, 37, 10, 2),
(75, 37, 11, 1),
(76, 38, 4, 1),
(77, 38, 10, 1),
(78, 38, 11, 1),
(79, 39, 1, 1),
(80, 39, 4, 1),
(81, 39, 10, 1),
(82, 39, 11, 1),
(83, 39, 17, 1),
(84, 39, 16, 1),
(85, 39, 13, 1),
(86, 40, 4, 1),
(87, 40, 10, 1),
(88, 40, 1, 1),
(89, 40, 11, 1),
(90, 40, 17, 1),
(91, 40, 16, 1),
(92, 41, 1, 1),
(93, 41, 4, 1),
(94, 41, 10, 1),
(95, 41, 11, 1),
(96, 41, 17, 1),
(97, 41, 16, 1),
(98, 42, 4, 1),
(99, 42, 10, 1),
(100, 42, 11, 1),
(101, 42, 1, 1),
(102, 42, 12, 1),
(103, 43, 1, 1),
(104, 43, 4, 1),
(105, 43, 11, 1),
(106, 43, 10, 1),
(107, 44, 4, 1),
(108, 45, 4, 3),
(109, 46, 4, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payment_voucher`
--

CREATE TABLE `payment_voucher` (
  `id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `paymentDate` datetime NOT NULL,
  `shippingID` int(11) NOT NULL,
  `inwardSlipVoucherId` int(11) NOT NULL,
  `isDelete` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `permission`
--

INSERT INTO `permission` (`id`, `name`) VALUES
(1, 'AAA'),
(5, 'BBB'),
(7, 'CCC');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_detail`
--

CREATE TABLE `permission_detail` (
  `userID` int(11) NOT NULL,
  `permissionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `permission_detail`
--

INSERT INTO `permission_detail` (`userID`, `permissionID`) VALUES
(13, 1),
(19, 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `categoryId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `thumbnail` text NOT NULL,
  `view` int(11) NOT NULL,
  `isActivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `post`
--

INSERT INTO `post` (`id`, `userId`, `title`, `summary`, `content`, `categoryId`, `dateCreated`, `thumbnail`, `view`, `isActivated`) VALUES
(10, 8, 'Yến sào Khánh Hòa tại Hải Phòng – Món ăn tốt cho sức khỏe của cả gia đình', 'Shop cung cấp Yến sào uy tín, với các cam kết chất lượng chuẩn nhất: KHÔNG tẩy trắng – KHÔNG độn đường – KHÔNG hàng kém chất lượng – KHÔNG hàng giả - KHÔNG độn gelatin.', '<p>Yến sào Hoàng Yến tại Hải Phòng xin kính chào quý khách!!!<br>&nbsp;</p><p><a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a> chúng tôi xin trân thành cảm ơn quý khách hàng đã luôn đồng hành và tin tưởng trong suốt những năm vừa qua</p><p>&nbsp;</p><p>Với đội ngũ nhân viên tư vấn nhiệt tình, chăm sóc khách hàng chu đáo chúng tôi tự hào về chất lượng, uy tín: Shop cung cấp Yến sào uy tín, với các cam kết chất lượng chuẩn nhất: KHÔNG tẩy trắng – KHÔNG độn đường – KHÔNG hàng kém chất lượng – KHÔNG hàng giả - KHÔNG độn gelatin.</p><p>&nbsp;</p><p>Yến sào Hoàng Yến với 3 cơ sở tại Hải Phòng, 2 chi nhánh tại 2 thành phố lớn : Hà Nội, thành phố Hồ Chí Minh. Chúng tôi chuyên cung cấp các sản phẩm Yến Sào nguyên chất, chính hãng Yến sào Khánh Hòa như: Bạch Yến, Hồng Yến, Huyết Yến, Yến sào rút lông, chân Yến tươi, Yến sợ…Và một số sản phẩm Yến tươi chưng sẵn như: Yến chưng Đông Trùng Hạ Thảo, Yến chưng táo đỏ, Yến chưng hạt chia, Yến chưng hạt sen.</p><p>&nbsp;</p><p><a href=\"https://yenvietkhanhhoa.com/yen-sao-khanh-hoa-tai-hai-phong-mon-an-tot-cho-suc-khoe-cua-ca-gia-dinh-tt5780.html\"><strong>Công dụng của Yến sào</strong></a><strong>:</strong></p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cong-dung-yen-sao.jpg\"></figure><p><i><strong>Đối với người già</strong></i>:</p><ul><li>Bổ sung dinh dưỡng cho cơ thể mà các bữa ăn hàng ngày không tích hợp hết được.</li><li>Tốt cho người mắc các bệnh về tim mạch như : mỡ máu, tiểu đường, huyết áp…</li><li>Cải thiện xương khớp đang lão hóa, giúp xương chắc khỏe</li><li>Thúc đẩy hoạt động của trí não, tăng cường trí nhớ.</li><li>Giúp cho tinh thân lạc quan, ăn ngon miệng, ngủ sâu giấc.</li></ul><p>&nbsp;</p><p><i><strong>Đối với trẻ nhỏ</strong></i>:</p><ul><li>Tăng sức đề kháng cho hệ miễn dịch, chống lại các tác nhân gây hại ngoài môi trường</li><li>Bổ sung dinh dưỡng cho trẻ phát triển chiều cao cân nặng của trẻ</li><li>Phát triển trí não giúp trẻ thông minh hơn</li><li>Kích thích tiêu hóa, hấp thụ và cải thiện tình trạng biếng ăn</li></ul><p><i><strong>Đối với phụ nữ mang bầu</strong></i>:</p><ul><li>Làm giảm một số triệu chứng thai nghén</li><li>Làm đẹp da</li><li>Tạo nền tảng cho con ra đời thông minh và mạnh khỏe</li><li>Hạn chế các vấn đề sau sinh</li></ul><p><i><strong>Đối với nam giới</strong></i>:</p><ul><li>Tái tạo tế bào<strong> </strong>giúp phát triển cơ bắp mạnh khỏe</li><li>Hỗ trợ xương chắc khỏe, hạn chế đau mỏi xương khớp</li><li>Ổn định tinh thần, hạn chế Stress căng thẳng, áp lực</li><li>Tăng cường sinh lý</li></ul><p>Yến sào có rất nhiều công dụng đối với chúng ta, đặc biệt là có công dụng làm tăng sức đề kháng, phòng chống các loại bẹnh về tim mạch, viêm phổi, tiểu đường … Hiện tại, đang là mùa dịch Covid 19 chúng ta phải luôn tăng cường cung cấp các dưỡng chất phù hợp để tăng cường miễn dịch, đẩy lùi dịch bệnh mà Yến sào có đầy đủ dưỡng chất để bạn tăng cường miễn dịch cho cơ thể.</p><p><a href=\"https://yenvietkhanhhoa.com/yen-sao-khanh-hoa-tai-hai-phong-mon-an-tot-cho-suc-khoe-cua-ca-gia-dinh-tt5780.html\"><strong>Chúng tôi có chương trình khuyến mãi nhằm khuyến khích mọi người sử dụng Yến sào để tăng cường sức đề kháng phòng chống dịch bệnh Covid 19</strong></a><br>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/02(1).jpg\"></figure><p>Các loại Yến sào có tại <a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a> :</p><ul><li>Bạch Yến:</li></ul><p>&nbsp;</p><ul><li>Hồng Yến:</li></ul><p>&nbsp;</p><p><a href=\"https://yenvietkhanhhoa.com/yen-sao-khanh-hoa-tai-hai-phong-mon-an-tot-cho-suc-khoe-cua-ca-gia-dinh-tt5780.html\"><strong>Các loại Yến tươi chưng sẵn</strong></a>:</p><ul><li>Yến chưng Hạt chia:</li></ul><p>&nbsp;</p><ul><li>Yến chưng Hạt chia đông trùng hạ thảo:</li></ul><p>&nbsp;</p><ul><li>Yến chưng Long nhãn:</li></ul><p>&nbsp;</p><ul><li>Yến chưng Táo đỏ đường phèn:</li></ul><p>&nbsp;</p><ul><li>Yến chưng Hạt sen:</li></ul><p>&nbsp;</p><ul><li>Yến chưng Đông Trùng Hạ thảo:</li></ul>', 2, '2021-06-07 00:00:00', 'image/02(1).jpg', 5, 1),
(11, 8, 'Người ăn chay có được ăn Yến sào không?', 'Yến sào có nguồn cung cấp dinh dưỡng tuyệt vời, rất tốt trong việc bồi bổ cho sức khỏe và chữa bệnh. Tuy nhiên, người ăn chay có ăn được Yến sào không, đó chính là điều thắc mắc của nhiều người.', '<p>Yến sào có nguồn cung cấp dinh dưỡng tuyệt vời, rất tốt trong việc bồi bổ cho sức khỏe và chữa bệnh. Tuy nhiên, người ăn chay có ăn được Yến sào không, đó chính là điều thắc mắc của nhiều người.</p><p>&nbsp;</p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/12.jpg\"></figure><p>Yến sào (Tổ yến) có nguồn gốc từ thiên nhiên. Nó được tạo thành bởi dãi của chim yến khi làm tổ, được hình thành trong khoảng thời gian 3 – 4 tháng dể đẻ con. Thông thường chim yến sẽ kiếm ăn vào ban ngày, ban đêm sẽ về làm tổ. Vậy nên tổ yến hoàn toàn không phải được làm từ thịt của gia súc, gia cầm và hải sản như nhiều người nghĩ. Thành phần có trong tổ yến có chứa rất nhiều Vitamin, Protein, chất đạm và các vi chất khác rất tốt cho sức khỏe.</p><p>&nbsp;</p><p>Trong Yến sào hoàn toàn không chứa các chất béo của động vật, quá trình khai thác không diễn ra quá trình giết mổ động vật. Vậy nên những người ăn chay có thể hoàn toàn yên tâm, các bạn có thể ăn được món ăn này, nó rất phù hợp với các bạn đang ăn chay.</p><p>&nbsp;</p><p>Hiện nay, có rất nhiều các ăn chay khác nhau. Có những người ăn chay theo ngày nhất định (những người ăn tại gia đình), ví dụ: một tháng ăn vào các ngày rằm (ngày 15), ngày mùng 1, … hoặc có ăn chay vào những ngày cuối tuần thứ 7, chủ nhật … Cũng có những người ăn chay trường (các thầy chùa, nhà sư…), có nghĩa là khẩu phần ăn của họ hàng ngày luôn luôn là những phần ăn chỉ toàn là những món chay.</p><p>&nbsp;</p><p>Ăn chay thường có mặt tốt và có mặt không, nhưng đối với những người có sức khỏe kém, mới ốm dậy, ăn chay có thể khiến cơ thể bị thiếu hụt các chất dinh dưỡng. Vì vậy, bổ sung thêm Yến sào vào trong thực đơn các món ăn chay thì thực sự là rất tốt, Yến sào sẽ giúp cung cấp các chất thiết yếu cho cơ thể, đặc biệt là chất protein – rất cần thiết trong phục hồi sức khỏe của người bệnh, có nhiều trong Yến sào.</p><p>&nbsp;</p><p>Vậy các bạn biết được “Người ăn chay ăn yến rất tốt cho sức khỏe”, bổ sung thêm Yến sào vào các món ăn chay sẽ giúp bổ sung các chất cho cơ thể, bồi bổ cho sức khỏe, ngăn ngừa các bệnh tật hiệu quả.</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/24.jpg\"></figure><p>&nbsp;</p><p><a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a> cung cấp Yến sào uy tín với các cam kết chuẩn về chất lượng 5 KHÔNG: KHÔNG tẩy trắng – KHÔNG độn đường – KHÔNG hàng giả– KHÔNG hàng kém chất lượng – KHÔNG độn Galatin. 100% Yến chuẩn, cam kết không chất bảo quản, thơm ngon vừa miệng.</p><p>&nbsp;</p><p><a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a> :</p><p>&nbsp;</p><p>Cơ sở 1: Số 24 Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>&nbsp;</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>&nbsp;</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng</p><p>&nbsp;</p><p>Website: yensaohaiphong.xyz</p><p>&nbsp;</p><p>Hotline: 0938.699.797</p>', 2, '2021-06-07 00:00:00', 'image/nguoi-an-chay-title.jpg', 6, 1),
(12, 8, 'Yến sào – món quà thượng hạng', 'Chúng ta đều biết điến Yến sào với nhiều công dụng tốt về sức khỏe. Việc lựa chọn Yến sào là món quà tặng cho người thân, đồng nghiệp,… Thật là món quà có giá trị đặc biệt, có ý nghĩa rất cao thể hiện được tấm lòng của bạn đối với người nhận', '<p><strong>Yến sào – món quà thượng hạng</strong></p><p>&nbsp;</p><p>Chúng ta đều biết điến Yến sào với nhiều công dụng tốt về sức khỏe. Việc lựa chọn Yến sào là món quà tặng cho người thân, đồng nghiệp,… Thật là món quà có giá trị đặc biệt, có ý nghĩa rất cao thể hiện được tấm lòng của bạn đối với người nhận.</p><p>&nbsp;</p><p>Vậy tại sao Yến sào được coi là món quà thượng hạng?</p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/22.jpg\"></figure><p>Yến sào có một số tác dụng tốt cho sức khỏe như:</p><p>&nbsp;</p><ul><li>Bổ sung dưỡng chất thiết yếu cho cơ thể</li><li>Kích thích hệ tiêu hóa</li><li>Bổ máu, tốt cho bà bầu</li><li>Giúp xương chắc khỏe, bổ sung canxi</li><li>Tăng cường hệ miễn dịch cho mọi người</li><li>Giúp thai nhi phát triển toàn diện</li><li>Khắc phục tình trạng biếng ăn, lười ăn ở trẻ nhỏ</li><li>Tốt cho người bị tiểu đường, ung thư, người mới phẫu thuật</li><li>Giúp chị em chăm sóc da từ sâu bên trong, chống lão hóa da</li><li>Ở đàn ông, ngăn ngừa các bệnh về tim, gan, phổi… khỏe mạnh và dẻo dai hơn trong quan hệ tình dục</li></ul><p><strong>Món quà thượng hạng nên dành cho ai?</strong></p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/23.jpg\"></figure><p>Yến sao có rất nhiều công dụng mà lại còn phù hợp với mọi người. Vậy nên, chọn Yến làm món quà thượng hạng cho người thân trong gia đình, dành tặng cho sếp, tặng cho đồng nghiệp…. Tất cả đều phù hợp, mà thể hiện được tấm lòng của người tặng cũng như giá trị mà Yến mang lại</p><p>&nbsp;</p><p><strong>Lựa chọn Yến làm quà tặng như thế nào mới tốt</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/24.jpg\"></figure><p>Khi lựa chọn Yến để làm quà tặng bạn không nên chọn loại Yến thô nguyên tổ vẫn còn nguyên lông, nên chọn các loại Yến đã qua sơ chế để có tính thẩm mỹ cao trong gói quà của bạn. Hãy chọn những địa chỉ uy tín, chất lượng tốt để mua Yến làm quà tặng. Hãy đến với Yến Sào Hoàng Yến để được tư vấn và lựa chọn loại yến tốt nhất để làm quà tặng tốt nhất, tuyệt vời và có ý nghĩa nhất.</p>', 3, '2021-06-07 03:32:53', 'image/2.jpg', 2, 1),
(13, 8, 'Yến thô là gì? Lợi ích khi sử dụng yến thô Yến thô là gì? Lợi ích khi sử dụng yến thô', 'Yến thô là một trong những món quà vô giá mà thiên nhiên ban tặng cho con người. Vậy tổ yến thô là gì, có lợi ích như thế nào mà được nhiều người tìm kiếm, săn lùng vậy? Hãy cùng xem những thông tin Yến sào Hoàng Yến chia sẻ để hiểu rõ về vấn đề này.', '<p>Yến thô là loại tổ yến được những người thợ thu hoạch từ những vùng đảo, vùng núi đá treo leo hoặc nuôi tại gia đình. Tổ yến thô có màu trắng, còn nguyên lông chim, chưa qua làm sạch hay chế biến, còn nguyên chất tự nhiên. Vậy nên, mua tổ yến thô chính là mua tổ yến còn nguyên các chất dinh dưỡng tự nhiên nhất.</p><p>&nbsp;</p><p><strong>Yến thô là gì?</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/33.jpg\"></figure><p>Yến thô là loại tổ yến được những người thợ thu hoạch từ những vùng đảo, vùng núi đá treo leo hoặc nuôi tại gia đình. Tổ yến thô có màu trắng, còn nguyên lông chim, chưa qua làm sạch hay chế biến, còn nguyên chất tự nhiên. Vậy nên, mua tổ yến thô chính là mua tổ yến còn nguyên các chất dinh dưỡng tự nhiên nhất.</p><p>&nbsp;</p><p><strong>Giá trị dinh dưỡng có trong yến thô</strong></p><p>&nbsp;</p><p>Đây là một trong tám món ăn cao sang nhất trong nền ẩm thực, có giá trị dinh dưỡng cao đối với cơ thể của con người. Yến sào được chế biến từ yến thô. Theo các nhà nghiên cứu cho thấy hàm lượng dinh dưỡng có trong tổ yến sào rất cao:</p><ul><li>Vì là loại thô, chưa qua chế biến nên hàm lượng các chất dinh dưỡng có trong Yến Thô cao hơn so với các loại yến khác, hàm lượng Protein cao từ 4555%, trong đó có chứa 18 loại axitamin.</li><li>Có một số loại axitamin có hàm lượng rất cao như: proline chiếm 5,27%; aspartic acid chiếm 4,96%.</li><li>Hàm lượng Tyrosine và acid syalic chiếm 8,6% cao hơn so với các loại yến khác.</li></ul><p><strong>Lợi ích khi sử dụng yến thô</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/34.jpg\"></figure><p>Yến thô có rất nhiều công dụng không ngờ tới mà mọi người chưa tìm hiểu hết:</p><ul><li>Tăng khả năng hấp thụ Vitamin D từ ánh sáng mặt trời</li><li>Tyrosine và acid syalic có trong Yến sào giúp cho người bệnh nhanh hồi phục cơ thể bị tổn thương.</li><li>Hồi phục sức khỏe cho người bệnh, tăng cường trí nhớ, tăng cường các chất có lợi cho các dây thân kinh xung đột.</li><li>Đối với trẻ nhỏ thì Yến thô giúp cung cấp đầy đủ dinh dưỡng cần thiết cho cơ thể trong giai đoạn phát triển. Kích thích hệ tiêu hóa, giúp cho trẻ hết biếng ăn, tăng sức đề kháng cho cơ thể, tránh tình trạng bị suy dinh dưỡng, còi xương. Giúp tăng chiều cao và tăng chỉ số IQ cho trẻ nhỏ.</li><li>Đối với các quý ông, thường xuyên phải ngoại giao do tính chất của công việc thì yến thô lại càng có công dụng . Nó giúp giải độc rượu bia, hạn chế các bệnh về gan, phooit, tim mạch… do tác hại của thuốc lá, rượu bia gây ra.</li></ul><p>Thực chất Yến thô thực sự tốt nếu chúng ta mua hàng chính hãng, hàng tốt, không phải hàng giả, hàng bị pha chế. Vậy nên khi lựa chọn mua Yến thô, bạn nên chọn những cửa hàng uy tín, chất lượng tốt để được mua đúng loại Yến thô chất lượng tốt nhất với giá thành hợp lý.</p><p>&nbsp;</p><p><strong>Yến Sào Hoàng Yến </strong>– bán Yến sào chất lượng đi cùng uy tín đến mọi gia đình.</p><p>Với cam kết 5 KHÔNG: : KHÔNG tẩy trắng – KHÔNG độn đường – KHÔNG hàng kém chất lượng – KHÔNG hàng giả - KHÔNG độn gelatin.</p><p>&nbsp;</p>', 2, '2021-06-07 03:41:24', 'image/4.jpg', 0, 1),
(14, 19, 'Các loại sản phẩm từ Yến sào tại Nhà Yến Việt', 'Theo kinh nghiệm của các chuyên gia khai thác về Yến Sào thì: Tổ Yến càng ở sâu trong hang động ẩm và tối thì màu sắc càng đỏ rực. Tổ nằm ở gần lối vào cửa hang thì có màu trắng được gọi là Bạch yến (yến trắng), đi sâu vào bên trong hang là các tổ có màu vàng được gọi là Hồng yến, bên trong cùng là các tổ yến có màu đỏ rực được gọi là Huyết yến.', '<p><a href=\"https://yenvietkhanhhoa.com/cac-loai-san-pham-tu-yen-sao-tai-nha-yen-viet-tt5773.html\"><strong>Các loại sản phẩm từ Yến sào tại&nbsp;Hoàng Yến</strong></a></p><p>Các bạn đang quan tâm đến các sản phẩm về Yến sào. Sau đây <a href=\"https://royalsanest.com/\"><strong>Yến Sào Hoàng Yến</strong></a> &nbsp;sẽ giới thiệu cho các bạn những sản phẩm hiện có tại cửa hàng.</p><p>Theo kinh nghiệm của các chuyên gia khai thác về Yến Sào thì: Tổ Yến càng ở sâu trong hang động ẩm và tối thì màu sắc càng đỏ rực. Tổ nằm ở gần lối vào cửa hang thì có màu trắng được gọi là Bạch yến (yến trắng), đi sâu vào bên trong hang là các tổ có màu vàng được gọi là Hồng yến, bên trong cùng là các tổ yến có màu đỏ rực được gọi là Huyết yến.</p><p>&nbsp;</p><p><strong>1. </strong><a href=\"https://yenvietkhanhhoa.com/cac-loai-san-pham-tu-yen-sao-tai-nha-yen-viet-tt5773.html\"><strong>Bạch Yến</strong></a></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/04_2.jpg\"></figure><p><strong>Bạch yến</strong> (hay còn gọi là yến trắng) là một trong 3 loại tổ yến nổi tiếng hiện nay. Bạch yến là loại tổ yến được lấy từ nước dãi của chim yến.</p><p>Trong thành phần của bạch yến có đến 18 loại acid amin, một số có hàm lượng rất cao như Aspartic acid, Serine, Tyrosine, Phenylalanine, Valine, Arginine, Leucine…</p><p>Hàm lượng protein chiếm đến 50-60% có tác dụng kích thích tăng trưởng tế bào và biểu bì, giúp sửa chữa các tế bào bị tổn thương và tăng cường hệ thống miễn dịch.</p><p>Đặc biệt acid syalic và Tyrosine trong bạch yến có tác dụng giúp phục hồi nhanh cơ thể bệnh nhân ung thư sau xạ trị, hóa trị, bệnh nhân sau khi mổ (nhất là về phổi, thận)</p><p>&nbsp;</p><p><strong>&nbsp;2. </strong><a href=\"https://yenvietkhanhhoa.com/cac-loai-san-pham-tu-yen-sao-tai-nha-yen-viet-tt5773.html\"><strong>Hồng Yến</strong></a></p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/hong-yen.jpg\"></figure><p><strong>Hồng yến là </strong>một loại tổ làm từ nước dãi chim yến, trên các vách đá ngoài đảo xa, tuy nhiên khác với các loại tổ yến thông thường màu sắc tổ có thể màu vỏ quýt đến màu vàng lòng đỏ trứng gà.</p><p>Hiện nay hồng yến chỉ được tìm thấy ở các vách đá cheo leo ngoài đảo xa, hang đá dựng đứng, nguy hiểm. Việc khai thác hái tổ yến vô cùng khó khăn, nguy hiểm, tốn nhiều chi phí, số lượng cũng hiếm nên giá hồng yến khá cao so với Bạch Yến.</p><p>&nbsp;</p><p><strong>3. Huyết Yến</strong></p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/huyet-yen.jpg\"></figure><p><strong>Huyết yến</strong> là tổ yến thường được tìm thấy ở sâu bên trong hang động, các vách đá treo leo. Hiện nay hồng yến chỉ được tìm thấy ở các vách đá cheo leo ngoài đảo xa, hang đá dựng đứng, nguy hiểm. Việc khai thác hái tổ yến vô cùng khó khăn, nguy hiểm, tốn nhiều chi phí, số lượng cũng hiếm nên giá hồng yến khá cao.</p><p>Bên cạnh 3 loại <a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a><a href=\"https://yenvietkhanhhoa.com/\"><strong>&nbsp;</strong></a>còn có thêm một số sản phẩm và dịch vụ khác liên qun đến chế biến Yến Sào:</p><ul><li>Yến chưng sẵn:Yến sào được nhặt hết lông và chế biến chưng đóng hộp, khi sử dụng chỉ cần dùng trực tiếp. Tiện lợi và phù hợp với những người có ít thời gian rảnh mà vẫn muốn sử dụng yến để bổ sung các chất cho cơ thể.</li><li>Nhụy hoa nghệ tây: hay còn được gọi là Saffron – đây được mệnh danh là báu vật của phụ nữ với nhiều công dụng như: làm trẻ hóa, làm da đẹp,điều hòa khí huyết….</li><li>Đông trùng hạ thảo: là một loại thảo dược xuất hiện nhiều tại vùng cao nguyên Tây Tạng. Nơi đây có khí hậu, thổ nhưỡng cũng như môi trường tuyệt vời để loại dược liệu này sinh trưởng. Theo các nhà khoa học trên thế giới có đến hơn 500 loại trùng thảo, tuy nhiên chỉ có duy nhất một loại được gọi là đông trùng hạ thảo (Cordyceps Sinensis) bởi sự hình thành vô cùng đặc biệt. Với các dạng sau đây:</li></ul><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; +&nbsp;Nước uống đông trùng hạ thảo: bao gồm 2 loại nước uống đông trùng hạ thảoHector Collagen và nước uống đông trùng hạ thảo Hector Sâm</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; + <a href=\"https://royalsanest.com/dong-trung-tui-nuoc-han-quoc-sp482015.html\"><strong>Đông trùng tùi nước Hàn Quốc</strong></a></p><ul><li>Dịch vụ nhặt lông yến: nhận nhặt lông yến từ tổ yến thô.</li><li>Và một số thực phẩm có thể kết hợp khi nấu yến sào như: <a href=\"https://royalsanest.com/tao-do-tan-cuong-sp482013.html\"><strong>Táo đỏ tân cương</strong></a>, Hắc kỳ&nbsp;tử, Kỳ tử đỏ, <a href=\"https://royalsanest.com/bo-gan-han-quoc-sp482012.html\"><strong>Bổ gan Hàn Quốc</strong></a>, <a href=\"https://royalsanest.com/nam-linh-chi-cao-cap-sp482014.html\"><strong>Nấm Linh Chi Cao Cấp</strong></a></li></ul><p>&nbsp;</p>', 2, '2021-06-07 10:16:53', 'image/to-yen-tinh-che-loai-1_.jpg', 5, 1),
(15, 19, 'Cách dùng Yến sào hợp lý', 'Các bạn đã tìm hiểu qua về các công dụng và lợi ích khi sử dụng Yến sào nhưng chưa biết sử dụng liều lượng như thế nào cho đúng, cho hiệu quả. Sau đây là chia sẻ về cách dùng Yến sào cho tất cả mọi người.', '<p>Các bạn đã tìm hiểu qua về các công dụng và lợi ích khi sử dụng Yến sào nhưng chưa biết sử dụng liều lượng như thế nào cho đúng, cho hiệu quả. Sau đây là chia sẻ về cách dùng Yến sào cho tất cả mọi người.</p><h2><a href=\"https://yenvietkhanhhoa.com/cach-dung-yen-sao-tt5775.html\"><strong>Liều dùng</strong></a></h2><ul><li>Đối với trẻ em 1 – 4 tuổi: nên ăn 1 – 2 gram/ngày</li><li>Đối với trẻ trên 4 tuổi trở lên: nên ăn 2 – 3 gram/ngày</li><li>Phụ nữ mang thai: 2 – 3 gram/ngày</li><li>Đàn ông, con trai thường xuyên hoạt động mạnh: 2 – 3 gram/ngày</li><li>Người già, ốm yếu, mới ốm dậy..: nên ăn 3 – 4 gram/ngày</li></ul><p><a href=\"https://yenvietkhanhhoa.com/cach-dung-yen-sao-tt5775.html\"><strong>Ăn Yến sào lúc nào là tốt nhất?</strong></a></p><p>Thời điểm tốt nhất, tuyệt vời nhất đó là ăn trước khi đi ngủ 1 tiếng, đây là giờ vàng để cơ thể bạn có thể hấp thụ các chất dinh dưỡng quý có trong Yến sào. Vì trong lúc bạn ngủ, nồng độ nội tiết tố trong cơ thể bạn tăng lên rất cao và thời điểm này cũng là thời điểm để cho bạn thải độc tố, thanh lọc cơ thể. Những yếu tố trên sẽ hỗ trợ cho bạn hấp thụ dưỡng chất có trong tổ yến nhiều hơn.</p><p>Ngoài ra, nếu bạn có điều kiện có thể cho người thân hay người trong gia đình mình dùng trước lúc ăn sáng 1 tiếng, vừa ngủ dậy ăn liền. Lúc đó trong bụng còn rỗng, khi đó dùng Yến hấp thụ sẽ rất tốt và dễ đề phòng các bệnh về tim mạch.</p><h2><a href=\"https://royalsanest.com/cach-dung-yen-sao-tt5775.html\"><strong>Những lưu ý khi sử dụng Yến sào</strong></a></h2><p>Bạn không nên sử dụng quá nhiều, sử dụng tùy ý Yến sào như lúc thích thì chưng ăn quá nhiều, ăn cho thỏa sức, rồi bỏ nhiều ngày mới ăn lại. Đó chính là định kiến sai lầm rất lớn khi sử dụng Yến sào. Dùng Yến sào là phải dùng đều đặn, chứ không phải dùng quá nhiều. Các bạn tốt nhất nên đưa ra lịch ăn hàng ngày, đều đặn và lên tục thì lúc đó bạn mới có thể cảm nhận được những thay đổi tích cực và những chuyển biến trên cơ thể bạn nhờ những chất tinh túy có trong thành phần của Yến Sào mang lại cho bạn và người thân.</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cach-dung-yen01.jpg\"><figcaption>Cách dùng tổ yến</figcaption></figure><p><a href=\"https://yenvietkhanhhoa.com/cach-dung-yen-sao-tt5775.html\"><strong>Địa chỉ mua Yến sào uy tín, chất lượng tốt:</strong></a></p><p>&nbsp;</p><p><a href=\"https://royalsanest.com/\"><strong>Yến sào Hoàng Yến</strong></a> :</p><p>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p>', 2, '2021-06-07 11:16:18', 'image/cach-dung yen hop-ly.jpg', 2, 1),
(16, 19, 'Cách Chưng Tổ Yến', 'Yến Chưng tươi ngon, mà lại còn có lợi cho sức khỏe, đơn giản và dễ làm. Yến Sào Hoàng Yến  sẽ chia sẻ cho các bạn về cách chưng tổ yến đơn giản và dễ làm nhất.', '<p><strong>Chuẩn bị Yến</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/chung-to-yen02.jpg\"></figure><p>&nbsp;</p><p>Lựa chọn loại Yến phụ thuộc vào sở thích của bạn và lọi ích mà bạn muốn dùng (Bạch Yến, Hồng Yến, Tuyết Yến)</p><p>Nếu tổ yến thô, bạn ngâm nước rồi nhặt hết lông có trong tổ yến (làm sạch yến). Nếu bạn mua tổ yến đã qua sơ chế thì không cần thực hiện bước này .</p><p>Chưng yến có thể cho thêm các nguyên liệu khác như: Đông trùng hạ thảo, nhụy hoa nghệ tây, hạt chia, đường,… tùy vào khẩu vị của từng người&nbsp;</p><p><strong>Các bước chưng Yến:</strong></p><ul><li>Lấy lượng Yến phù hợp, thực hiện ngâm Yến với nước sạch.</li></ul><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Mỗi loại yến ngâm với thời gian khác nhau</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Bạch yến (Yến tổ trắng) : Ngâm từ 2-4 tiếng.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Hồng yến (Yến tổ vàng) : Ngâm từ 3-6 tiếng.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Huyết yến (Yến tổ đỏ) : Ngâm từ 4-8 tiếng.</p><ul><li>Cho nước vào nồi.</li><li>Cho Yến và các nguyên liệu khác vào chén hoặc hũ làm bằng sứ rồi cho vào nồi nước, bật lửa nhỏ riu riu.</li><li>Thời gian chưng tùy thuộc vào từng loại tổ yến :</li></ul><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Bạch Yến chưng 15-30 phút.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Hồng Yến chưng 15-30 phút.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;+ Huyết Yến chưng 20-40 phút.</p><p><i><strong>Lưu ý</strong></i> : - Nếu chưng tổ yến quá lâu thì sẽ rất dễ bị mềm, nát vụn hoặc tan trong nước.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- Tổ Yến càng trắng, càng mới thì thời gian chưng càng ít và ngược lại, yến càng già, càng lâu năm thì thời gian ngâm nước và chưng càng lâu.</p>', 2, '2021-06-07 12:55:50', 'image/chung-to-yen-title.jpg', 1, 1),
(17, 19, 'Yến chưng sẵn – giải pháp tốt cho người bận rộn', 'Các bạn muốn mua Yến mà chưa biết cách làm như thế nào? Các bạn không có thời gian làm yến nhưng vẫn muốn sử dụng chúng bởi thấy chúng có nhiều công dụng. Vậy nên, Yến Sào Hoàng Yến  xin giới thiệu cho các bạn một loại sản phẩm được làm từ Yến Sào đó chính là Yến Chung Tươi.', '<p>Các bạn muốn mua Yến mà chưa biết cách làm như thế nào? Các bạn không có thời gian làm yến nhưng vẫn muốn sử dụng chúng bởi thấy chúng có nhiều công dụng. Vậy nên, Yến Sào Hoàng Yến xin giới thiệu cho các bạn một loại sản phẩm được làm từ Yến Sào đó chính là Yến Chung Tươi.&nbsp;</p><p>Yến Chưng Tươi&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/yen-chung-san-title.jpg\"></figure><p>Là sản phẩm được chế biến từ Yến Sào qua các quy tình như nhặt lông, làm sạch, chưng yến, đóng hộp, quy trình đảm bảo an toàn thực phẩm.&nbsp;</p><p><strong>Yến chưng sẵn tại sao được coi là giải pháp tốt cho người bận rộn?</strong>&nbsp;</p><p>Bạn không có thời gian, bạn là một con người bận rộn nhưng bạn vẫn muốn thưởng thức món ăn hảo hạng được chế biến từ yến sợi nguyên chất nhưng vẫn giữ được độ tươi ngon. Yến Sào Hoàng Yến chúng tôi có thêm các sản phẩm yến chưng với đảm bảo: sử dụng yến nguyên chất, chất lượng tốt nhất, yến chưng có độ tươi ngon như chưng tại nhà. Yến chưng tươi sẵn có nhiều lý do để chúng ta lựa chọn mua, đặc biệt là đối với những người phụ nữ bận rộn.&nbsp;</p><p>1. Tiết kiệm được thời gian tối đa&nbsp;</p><p>Bạn là người bận rộn, sản phẩm tiết kiệm thời gian luôn là tâm điểm chú ý của ban. Sản phẩm Yến chưng sẵn có ưu điểm lớn nhất là bạn không phải tốn thời gian để nhặt lông yến, lựa chọn nguyên liệu để chế biến. Bạn chỉ cần đặt hàng, nhận hàng, bật nắp và sử dụng trực tiếp mà không cần thêm các quy trình chế biến nữa.&nbsp;</p><p>2. Sử dụng Yến nguyên chất 100%.</p><p>Một trong những ưu điểm đặc biệt nổi bật nhất của Yến chưng Nhà Việt Yến đó chính là bảo được hương vị tươi ngon như chưng tại nhà. Bạn sẽ được lựa chọn cách mĩ vị, lượng đường theo sở thích, nhu cầu của bạn.&nbsp;</p><p>3. An toàn cho sức khỏe</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/yen-chung-san01.jpg\"></figure><p>Sử dụng các sản phẩm từ Tổ yến các bạn luôn luôn chú ý đến vấn an toàn đối với sức khỏe, không chỉ bản thân dùng mà còn cả gia đình, người thân, sếp, đồng nghiệp… cùng sử dụng. Bởi “Sức khỏe chính là hạnh phúc” nên chúng tôi luôn mong muốn các bạn hạnh phúc. Vì vậy nên chúng tôi cam kết sẽ hoàn tiền 100% giá trị sản phẩm nếu bạn phát hiện chúng tôi có sử dụng các chất bảo quản, các chất phụ gia khác có trong các hũ Yến chưng được sản xuất từ Nhà Yến Việt.&nbsp;</p><p>4. Hiệu quả nhanh chóng&nbsp;</p><p>Chưng tổ yến tại nhà, nhiều người không để ý đến liều lượng. Điều này khiến cho chúng ta sử dụng Yến trong thời gian dài nhưng hiệu quả lại rất thấp. Vậy thì đừng lo, bởi mỗi hũ Yến chưng được Nhà Yến Việt sản xuất theo “tỉ lệ kim cương” với hàm lượng được các chuyên gia tư vấn cụ thể cho tình trạng của từng người. Chắc chắn rằng bạn sẽ cảm nhận được hiệu quả ngay sau tuần đầu tiên sử dụng Yến Chưng tại Yến Sào Hoàng Yến . Địa chỉ liên hệ: Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng. Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng. Hotline: 0904224563 Website: yensaohaiphong.xyz</p>', 2, '2021-06-07 13:24:35', 'image/yen-chung-san-title.jpg', 1, 1),
(18, 8, ' Công dụng của yến sào', 'Yến sào là một trong 8 món “cao lương mỹ vị” hay “bát trân” , đây là loại thực phẩm có giá trị dinh dưỡng rất cao. Hiện nay có rất nhiều người sử dụng sản phẩm này nhưng chưa biết rõ công dụng của nó như thế nào? Hôm nay, Yến Sào Hoàng Yến sẽ chia sẻ cho bạn một số công dụng của yến sào đối với sức khỏe con người.', '<p>Yến sào là một trong 8 món “cao lương mỹ vị” hay “bát trân” , đây là loại thực phẩm có giá trị dinh dưỡng rất cao. Hiện nay có rất nhiều người sử dụng sản phẩm này nhưng chưa biết rõ công dụng của nó như thế nào? Hôm nay, Yến Sào Hoàng Yến sẽ chia sẻ cho bạn một số công dụng của yến sào đối với sức khỏe con người.&nbsp;</p><p><strong>Đối với người già&nbsp;</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cong-dung-yen-sao-nguoi-gia.png\"><figcaption>Công dụng của Yến sào với người già</figcaption></figure><p>Yến sào có công dụng chính là bồi bổ sức khoẻ và tăng cường sinh lực. Trong yến sào có chứa 18 loại acid amin và rất giàu Proline (5.27 %), Axit aspartic (4.69 %), nhiều nguyên tố quý như Ca, Fe, Mn, Br, Cu, Zn, Cr,…nhờ vậy mà ăn yến sào sẽ giúp cho hệ tiêu hoá tốt hơn, làm tăng lượng hồng cầu trong máu, kích thích các tế bào sinh trưởng và phục hồi các tế bào bị tổn thương. Chính vì vậy mà yến sào rất thích hợp cho người cao tuổi, những người dùng để bồi bổ cơ thể và phục hồi sức khoẻ.&nbsp;</p><ul><li>Bổ sung dinh dưỡng cho cơ thể mà các bữa ăn hàng ngày không tích hợp hết được.&nbsp;</li><li>Tốt cho người mắc các bệnh về tim mạch như : mỡ máu, tiểu đường, huyết áp…&nbsp;</li><li>Cải thiện xương khớp đang lão hóa, giúp xương chắc khỏe&nbsp;</li><li>Thúc đẩy hoạt động của trí não, tăng cường trí nhớ.&nbsp;</li><li>Giúp cho tinh thân lạc quan, ăn ngon miệng, ngủ sâu giấc.</li></ul><p><strong>Đối với trẻ em</strong>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cong-dung-yen-sao-voi-tre-em.png\"><figcaption>Công dụng của Yến sào với trẻ em</figcaption></figure><p>Theo các nhà nghiên cứu cho thấy, tổ yến sào có thành phần dưỡng chất rất quý giá đối với sức khỏe con người. Với đối tượng là trẻ em suy dinh dưỡng thì việc bổ sung yến sào là rất cần thiết. Bởi trong thành phần dinh dưỡng của yến sào có chứa Axit Amin là chất cần thiết cho sự hấp thu thức ăn của trẻ.&nbsp;</p><p>Đối với trẻ bị suy dinh dưỡng, hay mắc các bệnh do môi trường thay đổi thì việc dùng yến sào sẽ giúp tăng cường hệ miễn dịch cho trẻ, giúp trẻ hấp thu và tiêu hóa tốt hơn. Trẻ được dùng yến sào thường xuyên sẽ chóng lớn, tăng cường kích thích sinh trưởng của các tế bào…&nbsp;</p><ul><li>Tăng sức đề kháng cho hệ miễn dịch, chống lại các tác nhân gây hại ngoài môi trường&nbsp;</li><li>Bổ sung dinh dưỡng cho trẻ phát triển chiều cao cân nặng của trẻ&nbsp;</li><li>Phát triển trí não giúp trẻ thông minh hơn&nbsp;</li><li>Kích thích tiêu hóa, hấp thụ và cải thiện tình trạng biếng ăn&nbsp;</li></ul><p><strong>Đối với phụ nữ mang thai&nbsp;</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cong-dung-yen-sao-phu-nu-mang-thai.png\"><figcaption>Công dụng của Yến sào với phụ nữ mang thai</figcaption></figure><p>Trong thành phần của yến sào có nhiều chất dinh dưỡng, hàm lượng chất đạm rất cao cùng với nhiều axit amin, các vitamin và chất khoáng cần thiết cho cơ thể. Ăn yến sào có tác dụng bồi bổ cơ thể, tăng cường thể lực, tăng sức đề kháng, tăng cường hoạt động của hệ tiêu hóa giúp ăn ngon hơn, hệ hô hấp, hệ thần kinh….&nbsp;</p><p>Đặc biệt với bà bầu, yến sào cung cấp 18 axit amin và nhiều protein, với các chất khoáng như Mg, Sắt, Kẽm… là nguồn dinh dưỡng thiết yếu cho bà bầu và thai nhi.&nbsp;</p><ul><li>Làm giảm một số triệu chứng thai nghén như: mệt mỏi, hoa mắt, chóng mặt…&nbsp;</li><li>Làm đẹp da: Khi mang bầu người phụ nữ thường bị thay đổi nội tiết tố làm cho làn da xấu đi, bị nám. Với chất Threonie (2.69%), đây là chất bổ sung rất lớn trong việc hình thành nên Collagen và Elastinetrong, giúp tái tạo tế bào mô, cơ, làm cho da dẻ hồng hào, chống rạn da trong quá trình phù nề.&nbsp;</li><li>Tạo nền tảng cho con ra đời thông minh và mạnh khỏe: Các Acid Amin và Glycine có trong yến sào làm giảm nguy cơ tiền sản giật một căn bệnh vô cùng nguy hiểm ở bà bầu, nguy cơ khuyết tật ống thần kinh ở thai nhi cũng được giảm đi, giúp trẻ phát triển kháng thể hồi phục sau sinh cho người mẹ. Đó cũng là một tiền chất của Serotonin và Melatonin cần thiết cho sự tăng trưởng và phát triển tối ưu của trẻ và giúp cân bằng chất Nitrogen cho mẹ bầu</li></ul><p><strong>Đối với nam giới&nbsp;</strong></p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/cong-dung-yen-sao-nam-gioi.png\"><figcaption>Công dụng của Yến sào đối với nam giới</figcaption></figure><p>Từ ngàn xưa, yến sào đã được xem là một món ăn Cao Lương Mỹ Vị chỉ có những bậc vua chúa, những gia đình quyền quý mới có thể dùng. Tuy nhiên, ngày nay mô hình nuôi yến được nhân rộng nhiều nơi, việc sử dụng Yến sào không còn xa xỉ, quý ông dễ dàng có thể đảm bảo sức khỏe từ tổ yến:&nbsp;</p><ul><li>Tái tạo tế bào giúp phát triển cơ bắp mạnh khỏe&nbsp;</li><li>Hỗ trợ xương chắc khỏe, hạn chế đau mỏi xương khớp</li></ul><p>&gt;&gt;&gt; Yến sào có rất nhiều công dụng tốt đối sức khỏe, sử dụng&nbsp;Yến sào thương xuyên để bồi bổ dinh dưỡng cho cơ thể. Hãy liên hệ với Nhà Yến Việt để được tư vấn lựa chọn các loại sản phẩm Yến sào phù hợp nhất với bạn.&nbsp;</p><p>Yến Sào Hoàng Yến cung cấp Yến Sào uy tín, với các cam kết chất lượng chuẩn nhất: KHÔNG tẩy trắng – KHÔNG độn đường – KHÔNG hàng kém chất lượng – KHÔNG hàng giả - KHÔNG độn gelatin</p>', 2, '2021-06-07 17:19:42', 'image/cong-dung-cua-yen-sao.jpg', 5, 1),
(19, 8, 'Yến sào Nha Trang Khánh Hòa Hải Phòng - Yến sào nguyên chất Hoàng Yến', 'Thông tin về yến sào Hoàng Yến\nYẾN SÀO KHÁNH HOÀ HOÀNG YẾN\n\n“VÀNG TRẮNG” CỦA NƯỚC VIỆT\n\n10 tặng 1. Mua 1 lạng yến tặng 1 set yến đông trùng trị giá 510k\n\nĐổi trả trong vòng 30 ngày', '<p><strong>YẾN SÀO HOÀNG YẾN tại</strong> Hải Phòng xin kính chào quý khách!!! Chúng tôi xin trân trọng cảm quý khách đã luôn tin tưởng sử dụng sản phẩm của chúng tôi trong nhiều năm qua. Mời quý khách liên hệ <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được phục vụ&nbsp;VỚI HỆ THỐNG CÁC CỬA HÀNG CỦA YẾN SÀO HOÀNG YẾN – CHÚNG TÔI LUÔN CUNG CẤP CHẤT LƯỢNG VÀ DỊCH VỤ TỐT NHẤT TỚI KHÁCH HÀNG</p><p>Cơ sở 1: Số 24b Đà Nẵng, Ngô Quyền, Hải Phòng – vị trí đường Đà Nẵng nối giữa ngã 5 và ngã 6</p><p>&nbsp;</p><p>Hotline: <strong>0938.699.797&nbsp;</strong></p><p>Được nuôi dưỡng bằng niềm đam mê các phẩm vật vô giá từ thiên nhiên, cùng với tầm nhìn mong muốn đưa tới Thượng Khách của mình những sản phẩm chất lượng nhất – dịch vụ tốt nhất, HOÀNG YẾN là đơn vị hàng đầu cung cấp và phân phối TỔ YẾN SÀO KHÁNH HOÀ CHẤT LƯỢNG NHẤT TẠI HẢI PHÒNG.</p><p><strong>Những thượng phẩm mà chúng tôi cung cấp:</strong></p><ul><li>Yến thô Khánh Hoà</li><li>Yến tinh chế thượng hạng</li><li>Hồng yến tinh chế</li><li>Yến sào rút lông thượng hạng</li><li>Yến sào định hình</li><li>Nước yến sào chưng sẵn với các vị : Nước Yến Đông Trùng, Nước Yến cho trẻ em, Nước yến chưng đường phèn, Nước yến Nhân Sâm, Nước yến ngũ vị.</li></ul><p>1. Tại hệ thống HOÀNG YẾN, chúng tôi có nhận và trả đơn hàng ngày với các đơn hàng yến chưng sẵn từ YẾN TƯƠI TỪ TỔ YẾN SÀO: Yến chưng ngũ vị, Yến chưng đông trùng hạ thảo, yến chưng đường phèn, Yến chưng táo đỏ hạt chia, Chè yến, soup yến, cháo yến… theo yêu cầu của khách hàng. Chỉ cần khách hàng đặt gọi điện trước, trong vòng 60 phút chúng tôi sẽ giao tận tay khách hàng khi Yến chưng còn nóng hổi!</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y4.jpg\"></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y5.jpg\" alt=\"\"></figure><p>2. Đặc biệt, HOÀNG YẾN cho ra sản phẩm mới là YẾN SÀO CHƯNG SẴN với hương vị và công thức đặc biệt, chỉ cần một hũ/ngày là đủ cung cấp dưỡng chất cần thiết cho cơ thể. Đây là dòng sản phẩm vô cùng tiện ích và bổ dưỡng, phù hợp cho mọi người và cũng là món quà biếu tặng vô cùng hợp lý. Nhân dịp ra mắt, HOÀNG YẾN áp dụng chương trình <strong>MUA 10 TẶNG 1</strong> với <strong>400.000đ</strong> – trong suốt thời gian từ tháng 12/2020 đến tháng 2/2021.</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được tư vấn và giao hàng toàn quốc.</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y6.jpg\"></figure><p>3. Một THẾ MẠNH của HOÀNG YẾN chính là TỔ YẾN SÀO: với các sản phẩm từ Hồng Yến, Yến tinh chế Thượng hạng, Yến tinh chế bạch yến – chúng tôi cam kết những sản phẩm luôn CHẤT LƯỢNG nhất, giá cả HỢP LÝ nhất, dịch vụ chăm sóc khách hàng TỐT NHẤT.</p><p>Những sản phẩm yến sào của HOÀNG YẾN đã được khách hàng không chỉ Hải Phòng nói riêng mà cả những khách hàng ở những tỉnh thành khác rất ưa chuộng.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y7.jpg\"></figure><p>Chúng tôi cam kết các sản phẩm Yến sào <strong>KHÔNG</strong> tẩy trắng, <strong>KHÔNG</strong> tẩm đường, <strong>KHÔNG</strong> độn gelatin, <strong>KHÔNG</strong> có hàng giả, <strong>KHÔNG</strong> có hàng kém chất lượng, <strong>KHÔNG</strong> tạo ẩm.</p><p>Chúng tôi cam kết đổi trả hàng nếu khách hàng không ưng ý về chất lượng sản phẩm</p><p>Chúng tôi cam kết hàng giả sẽ đền bù 100 lần giá trị hàng khách đã thanh toán</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a><strong> </strong>để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><p>4. HOÀNG YẾN cung cấp <strong>set yến sào</strong> ăn trong vòng <strong>1 tuần</strong> chỉ với 399.000 đ</p><p>Một set gồm có tổ yến tinh chế thượng hạng cùng những vị để mix khi chưng yến như đường phèn, táo đỏ, hạt chia, kỷ tử, long nhãn. Rất tiện lợi cho khách hàng, ngoài ra lại kinh tế và tiện mua theo tuần.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y8.jpg\"></figure><p>&nbsp;</p><p>&nbsp;</p><p><strong>4. TỔ YẾN TINH CHẾ KHÁNH HOÀ THƯỢNG HẠNG</strong></p><p>Yến tinh chế là dòng yến tiện dụng, hiệu quả sử dụng trông thấy khi sử dụng 1 tuần, giá thành cũng hợp lý, sợi yến ngon, nở, mùi thơm đặc trưng của Yến làm cho khách hàng rất ưng ý. Cũng rất nhiều người đã lựa chọn yến tinh chế để làm quà biếu tặng cho ba mẹ, người thân, đối tác, bạn bè.</p><p>Yến tinh chế của HOÀNG YẾN được lựa chọn từ nhưng tổ yến già dày sợi, tai A, hàm lượng dinh dưỡng cao với đầy đủ các axit amin và các dưỡng chất. Yến thô loại 1 được những thợ yến lành nghề nhất của Hoàng yến ngâm trong nước sạch, sau đó dùng nhíp chuyên dùng để làm sạch lông và tạp chất, sau đó sấy khô và cho ra những tổ yến tinh chế thượng hạng. Yến tinh chế này 100% tự nhiên, chúng tôi cam kết&nbsp; không tẩy trắng, không tẩm đường, không độn gelatin, không có hàng giả, không có hàng kém chất lượng, không tạo ẩm.</p><p>Khi Khách hàng mua Yến tinh chế Khánh Hoà thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y9.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>4.600.000</strong></td><td><strong>2.300.000</strong></td></tr></tbody></table></figure><p><strong>5. TỔ YẾN THÔ THƯỢNG HẠNG</strong></p><p>Những tổ yến thô HOÀNG YẾN đưa đến tay khách hàng luôn được chọn lọc kỹ càng, tổ dáng tai A, già tổ, dày mình, ít bụng, ít lông và phân rêu. Tuy nhiên để chế biến món ăn thì yến thô cần được làm sạch sẽ, HOÀNG YẾN có nhận dịch vụ làm sạch tổ yến thô cho những khách hàng bận rộn</p><p>Khi Khách hàng mua Yến thô thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y10.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>2.700.000</strong></td><td><strong>1.350.000</strong></td><td><strong>810.000</strong></td></tr></tbody></table></figure><p>&nbsp;</p><p><strong>6. TỔ YẾN RÚT LÔNG/ RÚT LÔNG KHÔ THƯỢNG HẠNG</strong></p><p>Được chọn lọc từ những tổ yến thô đẹp nhất, sạch sẽ nhất, những người thợ lành nghề nhất của HOÀNG YẾN sẽ được giao nhiệm vụ rút lông theo phương pháp đặc biệt không ngâm nước để làm sạch tổ yến. Chính vì vậy, tổ yến rút lông sẽ không tổ nào giống tổ nào, chỉ toàn những sợi dài, và bởi do phương pháp làm sạch đặc biệt như vậy nên dưỡng chất của Tổ yến vô cùng cao.</p><p>Yến rút lông thượng hạng của HOÀNG YẾN luôn được lựa chọn là món quà tặng biếu cao cấp, đặc biệt khách hàng nước ngoài rất ưa chuộng dòng yến này. Sợi yến khi được chế biến nhìn đẹp mắt, nở vô cùng nhiều, hương vị đậm đà.</p><p>Khi Khách hàng mua Yến rút lông thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y11.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>5.200.000</strong></td><td><strong>2.600.000</strong></td><td><strong>1.560.000</strong></td></tr></tbody></table></figure><p>&nbsp;</p><p><strong>7. YẾN ĐẢO TINH CHẾ THƯỢNG HẠNG</strong></p><p>Những tổ yến đảo chất lượng và đẹp nhất được HOÀNG YẾN lựa chọn kỹ càng, sau đó được thợ lành nghề làm sạch sẽ, nhặt lộng, sấy khô và cho ra những tổ yến chất lượng nhất.</p><p>Với yến đảo, quý khách khi mua về ngâm, chưng và thưởng thức sẽ cảm nhận hương vị khó quên với độ dai ngon, thơm, nở của từng sợi yến.</p><p>Khi Khách hàng mua Yến đảo tinh chế thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y12.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>6.500.000</strong></td><td><strong>3.250.000</strong></td><td><strong>1.950.000</strong></td></tr></tbody></table></figure><p><strong>8. HỒNG YẾN TINH CHẾ</strong></p><p>Với sản lượng yến thì Hồng yến chỉ chiếm 1% nên quý hiếm hơn các loại Bạch yến khác, ngoài ra Hồng yến còn là mặt hàng cao cấp phù hợp với biếu tặng cho Thượng khách.</p><p>Khi Khách hàng mua Hồng Yến của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>5.800.000</strong></td><td><strong>2.900.000</strong></td><td><strong>1.740.000</strong></td></tr></tbody></table></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y13.jpg\"></figure><p>&nbsp;</p><p><strong>9. QUÀ TẶNG BIẾU DỊP TẾT</strong></p><p>Giỏ quà biếu tết của YẾN SÀO HOÀNG YẾN&nbsp;– không chỉ là một món quà vật chất, mà còn là món quà tinh thần, giúp bồi bổ và tăng cường sức khỏe cho chính những người thân yêu của bạn. HOÀNG YẾN với hàng trăm giỏ quà được thiết kế vô cùng sang trọng và tinh tế, thể hiện đẳng cấp vượt trội sẽ làm hài lòng những khách hàng khó tính nhất. Sản phẩm có&nbsp;Giá chỉ từ 1,5tr.</p><p>Hiện tại, HOÀNG YẾN đang có chương trình khuyến mãi – ĐẶT MUA từ GIỎ QUÀ THỨ 2 Quý khách sẽ ĐƯỢC TẶNG KÈM 01 NỒI CHƯNG YẾN đẹp siêu cấp trị giá <strong>300.000 đ</strong></p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y14.jpg\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y15.jpg\"></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y16.jpg\"></figure><p>VỚI HỆ THỐNG CÁC CỬA HÀNG CỦA YẾN SÀO HOÀNG YẾN – CHÚNG TÔI LUÔN CUNG CẤP CHẤT LƯỢNG VÀ DỊCH VỤ TỐT NHẤT TỚI KHÁCH HÀNG</p><p>Cơ sở 1: Số 24b Đà Nẵng, Ngô Quyền, Hải Phòng – vị trí đường Đà Nẵng nối giữa ngã 5 và ngã 6</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>&nbsp;</p><p>Hotline: <strong>0938.699.797</strong></p>', 3, '2021-06-07 22:16:30', 'image/y5.jpg', 9, 1),
(20, 8, 'Set Yến dành cho mùa Tết yêu thương', 'Yến sào Hoàng Yến tại Hải Phòng xin kính chào quý khách!!! Chúng tôi xin trân trọng cảm quý khách đã luôn tin tưởng sử dụng sản phẩm của chúng tôi trong nhiều năm qua.', '<p><strong>Những thượng phẩm mà chúng tôi cung cấp:</strong></p><ul><li>Yến thô Khánh Hoà</li><li>Yến tinh chế thượng hạng</li><li>Hồng yến tinh chế</li><li>Yến sào rút lông thượng hạng</li><li>Yến sào định hình</li><li>Nước yến sào chưng sẵn với các vị : Nước Yến Đông Trùng, Nước Yến cho trẻ em, Nước yến chưng đường phèn, Nước yến Nhân Sâm, Nước yến ngũ vị.</li></ul><p>1. Tại hệ thống HOÀNG YẾN, chúng tôi có nhận và trả đơn hàng ngày với các đơn hàng yến chưng sẵn từ YẾN TƯƠI TỪ TỔ YẾN SÀO: Yến chưng ngũ vị, Yến chưng đông trùng hạ thảo, yến chưng đường phèn, Yến chưng táo đỏ hạt chia, Chè yến, soup yến, cháo yến… theo yêu cầu của khách hàng. Chỉ cần khách hàng đặt gọi điện trước, trong vòng 60 phút chúng tôi sẽ giao tận tay khách hàng khi Yến chưng còn nóng hổi!</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y4.jpg\"></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y5.jpg\" alt=\"\"></figure><p>2. Đặc biệt, HOÀNG YẾN cho ra sản phẩm mới là YẾN SÀO CHƯNG SẴN với hương vị và công thức đặc biệt, chỉ cần một hũ/ngày là đủ cung cấp dưỡng chất cần thiết cho cơ thể. Đây là dòng sản phẩm vô cùng tiện ích và bổ dưỡng, phù hợp cho mọi người và cũng là món quà biếu tặng vô cùng hợp lý. Nhân dịp ra mắt, HOÀNG YẾN áp dụng chương trình <strong>MUA 10 TẶNG 1</strong> với <strong>400.000đ</strong> – trong suốt thời gian từ tháng 12/2020 đến tháng 2/2021.</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y6.jpg\"></figure><p>3. Một THẾ MẠNH của HOÀNG YẾN chính là TỔ YẾN SÀO: với các sản phẩm từ Hồng Yến, Yến tinh chế Thượng hạng, Yến tinh chế bạch yến – chúng tôi cam kết những sản phẩm luôn CHẤT LƯỢNG nhất, giá cả HỢP LÝ nhất, dịch vụ chăm sóc khách hàng TỐT NHẤT.</p><p>Những sản phẩm yến sào của HOÀNG YẾN đã được khách hàng không chỉ Hải Phòng nói riêng mà cả những khách hàng ở những tỉnh thành khác rất ưa chuộng.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y7.jpg\"></figure><p>Chúng tôi cam kết các sản phẩm Yến sào <strong>KHÔNG</strong> tẩy trắng, <strong>KHÔNG</strong> tẩm đường, <strong>KHÔNG</strong> độn gelatin, <strong>KHÔNG</strong> có hàng giả, <strong>KHÔNG</strong> có hàng kém chất lượng, <strong>KHÔNG</strong> tạo ẩm.</p><p>Chúng tôi cam kết đổi trả hàng nếu khách hàng không ưng ý về chất lượng sản phẩm</p><p>Chúng tôi cam kết hàng giả sẽ đền bù 100 lần giá trị hàng khách đã thanh toán</p><p>Quý khách hãy gọi <a href=\"tel:0938.699.797\"><strong>0938.699.797</strong></a><strong> </strong>để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><p>4. HOÀNG YẾN cung cấp <strong>set yến sào</strong> ăn trong vòng <strong>1 tuần</strong> chỉ với 399.000 đ</p><p>Một set gồm có tổ yến tinh chế thượng hạng cùng những vị để mix khi chưng yến như đường phèn, táo đỏ, hạt chia, kỷ tử, long nhãn. Rất tiện lợi cho khách hàng, ngoài ra lại kinh tế và tiện mua theo tuần.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y8.jpg\"></figure><p>&nbsp;</p><p>&nbsp;</p><p><strong>4. TỔ YẾN TINH CHẾ KHÁNH HOÀ THƯỢNG HẠNG</strong></p><p>Yến tinh chế là dòng yến tiện dụng, hiệu quả sử dụng trông thấy khi sử dụng 1 tuần, giá thành cũng hợp lý, sợi yến ngon, nở, mùi thơm đặc trưng của Yến làm cho khách hàng rất ưng ý. Cũng rất nhiều người đã lựa chọn yến tinh chế để làm quà biếu tặng cho ba mẹ, người thân, đối tác, bạn bè.</p><p>Yến tinh chế của HOÀNG YẾN được lựa chọn từ nhưng tổ yến già dày sợi, tai A, hàm lượng dinh dưỡng cao với đầy đủ các axit amin và các dưỡng chất. Yến thô loại 1 được những thợ yến lành nghề nhất của Hoàng yến ngâm trong nước sạch, sau đó dùng nhíp chuyên dùng để làm sạch lông và tạp chất, sau đó sấy khô và cho ra những tổ yến tinh chế thượng hạng. Yến tinh chế này 100% tự nhiên, chúng tôi cam kết&nbsp; không tẩy trắng, không tẩm đường, không độn gelatin, không có hàng giả, không có hàng kém chất lượng, không tạo ẩm.</p><p>Khi Khách hàng mua Yến tinh chế Khánh Hoà thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p>', 3, '2021-06-07 22:19:38', 'image/y15.jpg', 5, 1);
INSERT INTO `post` (`id`, `userId`, `title`, `summary`, `content`, `categoryId`, `dateCreated`, `thumbnail`, `view`, `isActivated`) VALUES
(21, 8, 'TỔ YẾN TINH CHẾ KHÁNH HOÀ THƯỢNG HẠNG', 'Yến tinh chế là dòng yến tiện dụng, hiệu quả sử dụng trông thấy khi sử dụng 1 tuần, giá thành cũng hợp lý, sợi yến ngon, nở, mùi thơm đặc trưng của Yến làm cho khách hàng rất ưng ý. Cũng rất nhiều người đã lựa chọn yến tinh chế để làm quà biếu tặng cho ba mẹ, người thân, đối tác, bạn bè.', '<p>Yến tinh chế của HOÀNG YẾN được lựa chọn từ nhưng tổ yến già dày sợi, tai A, hàm lượng dinh dưỡng cao với đầy đủ các axit amin và các dưỡng chất. Yến thô loại 1 được những thợ yến lành nghề nhất của Hoàng yến ngâm trong nước sạch, sau đó dùng nhíp chuyên dùng để làm sạch lông và tạp chất, sau đó sấy khô và cho ra những tổ yến tinh chế thượng hạng. Yến tinh chế này 100% tự nhiên, chúng tôi cam kết&nbsp; không tẩy trắng, không tẩm đường, không độn gelatin, không có hàng giả, không có hàng kém chất lượng, không tạo ẩm.</p><p>Khi Khách hàng mua Yến tinh chế Khánh Hoà thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y9.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>4.600.000</strong></td><td><strong>2.300.000</strong></td></tr></tbody></table></figure><p><strong>5. TỔ YẾN THÔ THƯỢNG HẠNG</strong></p><p>Những tổ yến thô HOÀNG YẾN đưa đến tay khách hàng luôn được chọn lọc kỹ càng, tổ dáng tai A, già tổ, dày mình, ít bụng, ít lông và phân rêu. Tuy nhiên để chế biến món ăn thì yến thô cần được làm sạch sẽ, HOÀNG YẾN có nhận dịch vụ làm sạch tổ yến thô cho những khách hàng bận rộn</p><p>Khi Khách hàng mua Yến thô thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y10.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>2.700.000</strong></td><td><strong>1.350.000</strong></td><td><strong>810.000</strong></td></tr></tbody></table></figure><p>&nbsp;</p><p><strong>6. TỔ YẾN RÚT LÔNG/ RÚT LÔNG KHÔ THƯỢNG HẠNG</strong></p><p>Được chọn lọc từ những tổ yến thô đẹp nhất, sạch sẽ nhất, những người thợ lành nghề nhất của HOÀNG YẾN sẽ được giao nhiệm vụ rút lông theo phương pháp đặc biệt không ngâm nước để làm sạch tổ yến. Chính vì vậy, tổ yến rút lông sẽ không tổ nào giống tổ nào, chỉ toàn những sợi dài, và bởi do phương pháp làm sạch đặc biệt như vậy nên dưỡng chất của Tổ yến vô cùng cao.</p><p>Yến rút lông thượng hạng của HOÀNG YẾN luôn được lựa chọn là món quà tặng biếu cao cấp, đặc biệt khách hàng nước ngoài rất ưa chuộng dòng yến này. Sợi yến khi được chế biến nhìn đẹp mắt, nở vô cùng nhiều, hương vị đậm đà.</p><p>Khi Khách hàng mua Yến rút lông thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y11.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>5.200.000</strong></td><td><strong>2.600.000</strong></td><td><strong>1.560.000</strong></td></tr></tbody></table></figure><p>&nbsp;</p><p><strong>7. YẾN ĐẢO TINH CHẾ THƯỢNG HẠNG</strong></p><p>Những tổ yến đảo chất lượng và đẹp nhất được HOÀNG YẾN lựa chọn kỹ càng, sau đó được thợ lành nghề làm sạch sẽ, nhặt lộng, sấy khô và cho ra những tổ yến chất lượng nhất.</p><p>Với yến đảo, quý khách khi mua về ngâm, chưng và thưởng thức sẽ cảm nhận hương vị khó quên với độ dai ngon, thơm, nở của từng sợi yến.</p><p>Khi Khách hàng mua Yến đảo tinh chế thượng hạng của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y12.jpg\"></figure><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>6.500.000</strong></td><td><strong>3.250.000</strong></td><td><strong>1.950.000</strong></td></tr></tbody></table></figure><p><strong>8. HỒNG YẾN TINH CHẾ</strong></p><p>Với sản lượng yến thì Hồng yến chỉ chiếm 1% nên quý hiếm hơn các loại Bạch yến khác, ngoài ra Hồng yến còn là mặt hàng cao cấp phù hợp với biếu tặng cho Thượng khách.</p><p>Khi Khách hàng mua Hồng Yến của HOÀNG YẾN sẽ được tặng nhíp nhổ chuyên dùng, táo đỏ, đường phèn, hạt chia, kỷ tử, long nhãn.</p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"table\"><table><tbody><tr><td><strong>HỘP</strong></td><td><strong>HỘP 100GR</strong></td><td><strong>HỘP 50GR</strong></td><td><strong>HỘP 30GR</strong></td></tr><tr><td><strong>GIÁ</strong></td><td><strong>5.800.000</strong></td><td><strong>2.900.000</strong></td><td><strong>1.740.000</strong></td></tr></tbody></table></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y13.jpg\"></figure><p>&nbsp;</p><p><strong>9. QUÀ TẶNG BIẾU DỊP TẾT</strong></p><p>Giỏ quà biếu tết của YẾN SÀO HOÀNG YẾN&nbsp;– không chỉ là một món quà vật chất, mà còn là món quà tinh thần, giúp bồi bổ và tăng cường sức khỏe cho chính những người thân yêu của bạn. HOÀNG YẾN với hàng trăm giỏ quà được thiết kế vô cùng sang trọng và tinh tế, thể hiện đẳng cấp vượt trội sẽ làm hài lòng những khách hàng khó tính nhất. Sản phẩm có&nbsp;Giá chỉ từ 1,5tr.</p><p>Hiện tại, HOÀNG YẾN đang có chương trình khuyến mãi – ĐẶT MUA từ GIỎ QUÀ THỨ 2 Quý khách sẽ ĐƯỢC TẶNG KÈM 01 NỒI CHƯNG YẾN đẹp siêu cấp trị giá <strong>300.000 đ</strong></p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y14.jpg\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y15.jpg\"></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y16.jpg\"></figure><p>VỚI HỆ THỐNG CÁC CỬA HÀNG CỦA YẾN SÀO HOÀNG YẾN – CHÚNG TÔI LUÔN CUNG CẤP CHẤT LƯỢNG VÀ DỊCH VỤ TỐT NHẤT TỚI KHÁCH HÀNG</p><p>Cơ sở 1: Số 24b Đà Nẵng, Ngô Quyền, Hải Phòng – vị trí đường Đà Nẵng nối giữa ngã 5 và ngã 6</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/telephone%20(1).png\" alt=\"☎\"></figure><p>Hotline: <strong>0938.699.797</strong></p><p>&nbsp;</p><p>Ngoài các sản phẩm từ Tổ yến sào, HOÀNG YẾN còn là nơi UY TÍN chuyên cung cấp Đông trùng hạ thảo Tây Tạng, sinh khối đông trùng tươi, Đông trùng sấy khô, nấm đông trùng, Nhuỵ hoa nghệ tây, Hắc kỷ tử Tây Tạng.. và nhiều mặt hàng Thực dưỡng khác.</p><p>&nbsp;</p><p>Sự lựa chọn của quý khách là niềm vinh hạnh của chúng tôi!</p><p>&nbsp;</p><p>Xin cám ơn quý khách</p>', 3, '2021-06-07 22:21:42', 'image/y10.jpg', 0, 1),
(22, 8, 'QUÀ TẶNG BIẾU DỊP TẾT', 'Giỏ quà biếu tết của YẾN SÀO HOÀNG YẾN – không chỉ là một món quà vật chất, mà còn là món quà tinh thần, giúp bồi bổ và tăng cường sức khỏe cho chính những người thân yêu của bạn. HOÀNG YẾN với hàng trăm giỏ quà được thiết kế vô cùng sang trọng và tinh tế, thể hiện đẳng cấp vượt trội sẽ làm hài lòng những khách hàng khó tính nhất. Sản phẩm có Giá chỉ từ 1,5tr.', '<p>Hiện tại, HOÀNG YẾN đang có chương trình khuyến mãi – ĐẶT MUA từ GIỎ QUÀ THỨ 2 Quý khách sẽ ĐƯỢC TẶNG KÈM 01 NỒI CHƯNG YẾN đẹp siêu cấp trị giá <strong>300.000 đ</strong></p><p>Quý khách hãy gọi <strong>0938.699.797</strong> để được tư vấn và giao hàng toàn quốc.</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y14.jpg\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y15.jpg\"></figure><figure class=\"image\"><img src=\"https://royalsanest.com/Data/images/y16.jpg\"></figure><p>VỚI HỆ THỐNG CÁC CỬA HÀNG CỦA YẾN SÀO HOÀNG YẾN – CHÚNG TÔI LUÔN CUNG CẤP CHẤT LƯỢNG VÀ DỊCH VỤ TỐT NHẤT TỚI KHÁCH HÀNG</p><p>Cơ sở 1: Số 24b Đà Nẵng, Ngô Quyền, Hải Phòng – vị trí đường Đà Nẵng nối giữa ngã 5 và ngã 6</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>&nbsp;</p><p>Hotline: <strong>0938.699.797&nbsp;</strong></p><p>&nbsp;</p><p>Ngoài các sản phẩm từ Tổ yến sào, HOÀNG YẾN còn là nơi UY TÍN chuyên cung cấp Đông trùng hạ thảo Tây Tạng, sinh khối đông trùng tươi, Đông trùng sấy khô, nấm đông trùng, Nhuỵ hoa nghệ tây, Hắc kỷ tử Tây Tạng.. và nhiều mặt hàng Thực dưỡng khác.</p><p>&nbsp;</p><p>Sự lựa chọn của quý khách là niềm vinh hạnh của chúng tôi!</p><p>&nbsp;</p><p>Xin cám ơn quý khách!</p>', 3, '2021-06-07 22:23:14', 'image/y16.jpg', 7, 1),
(23, 8, 'Làm sao để trả hàng khi tôi nhận sai hàng ?', 'Người mua được trả lại hàng nếu hàng hóa khác với mô tả; Nhận sai hàng; Thiếu hàng; Hàng đã qua sử dụng; Hàng giả/nhái; Hàng lỗi, không hoạt động hoặc Hàng bị bể/vỡ. Tuy nhiên người mua cần phải yêu cầu đổi trả hàng trong thời gian nhất định và phải được bên bán xác nhận.', '<p>Người mua được trả lại hàng nếu hàng hóa khác với mô tả; Nhận sai hàng; Thiếu hàng; Hàng đã qua sử dụng; Hàng giả/nhái; Hàng lỗi, không hoạt động hoặc Hàng bị bể/vỡ. Tuy nhiên người mua cần phải yêu cầu đổi trả hàng trong thời gian nhất định và phải được bên bán xác nhận.</p>', 4, '2021-06-09 00:58:04', 'image/returns_hang-hoa-2.jpg', 5, 1),
(24, 8, 'QUY ĐỊNH VỀ ĐÓNG GÓI HÀNG HÓA', 'Trước khi vận chuyển, Người Bán phải đảm bảo hàng hóa đã sẵn sàng để được vận chuyển với quãng đường tương ứng với từng đơn hàng. Cụ thể như sau:', '<p>&nbsp;Yêu cầu chung</p><ul><li>Tất cả các bưu kiện đều phải đóng gói sẵn sàng trước khi vận chuyển, được niêm phong bởi Người Bán. Đơn vị vận chuyển sẽ chỉ chịu trách nhiệm vận chuyển hàng hóa theo nguyên tắc “nguyên đai, nguyên kiện”, và sẽ không chịu trách nhiệm với nội dung hàng hóa bên trong nếu sản phẩm được giao tới tay Người Mua/hoàn về tay Người Bán trong tình trạng còn nguyên niêm phong và bao bì không bị rách/vỡ/ướt/móp méo.</li><li>Trên&nbsp;bao bì tất cả các bưu kiện đều phải có thông tin:<ul><li>Thông tin Người nhận, bao gồm: Tên người nhận, số điện thoại và địa chỉ người nhận</li><li>Mã vận đơn của đơn hàng</li><li>Ghi chú hàng không vận chuyển được bằng đường hàng không (nếu có ít nhất 01 sản phẩm trong bưu kiện nằm trong nhóm sản phẩm không thể vận chuyển bằng đường hàng không)</li><li>Mã đơn hàng trên&nbsp;hệ thống Shopee (không bắt buộc)</li></ul></li><li>Để đảm bảo an toàn cho hàng hóa, Shopee khuyến cáo Người Bán (Người Gửi) nên gửi kèm hóa đơn tài chính hợp lệ của sản phẩm trong bưu kiện (nếu có). Hóa đơn tài chính là căn cứ hỗ trợ quá trình xử lý khiếu nại như: xác định giá trị thị trường của hàng hóa, đảm bảo hàng hóa lưu thông hợp lệ, không bị tịch thu bởi cơ quan quản lý thị trường, v.v..</li><li>Đơn vị vận chuyển có quyền bóc mở bưu kiện để kiểm tra nội dung hàng hóa trong trường hợp nghi ngờ Người bán gửi sản phẩm không hỗ trợ vận chuyển hoặc có hành vi gửi hộp rỗng không chứa hàng</li></ul>', 4, '2021-06-09 01:20:16', 'image/logistics_preview-1.png', 4, 1),
(25, 8, 'Chính sách bảo mật thông tin khách hàng', 'Chính sách bảo mật sẽ giải thích cách tiếp nhận, sử dụng và (trong trường hợp nào đó) tiết lộ thông tin cá nhân của bạn. Chính sách cũng sẽ giải thích các bước hãng thực hiện để bảo mật thông tin cá nhân của khách hàng. Cuối cùng, chính sách bảo mật sẽ giải thích quyền lựa chọn của bạn về việc thu thập, sử dụng và tiết lộ thông tin cá nhân của mình.', '<p>Bảo vệ dữ liệu cá nhân và gây dựng được niềm tin cho bạn là vấn đề rất quan trọng với hãng. Vì vậy, The Face Shop&nbsp;sẽ dùng tên và các thông tin khác liên quan đến bạn tuân thủ theo nội dung của chính sách bảo mật. Họ&nbsp;chỉ thu thập những thông tin cần thiết liên quan đến giao dịch mua bán.</p><p>Họ&nbsp;sẽ giữ thông tin của khách hàng trong thời gian luật pháp quy định hoặc cho mục đích nào đó. Bạn có thể truy cập vào web và trình duyệt mà không cần phải cung cấp chi tiết cá nhân. Lúc đó, bạn đang ẩn danh và họ&nbsp;không thể biết bạn là ai nếu bạn không đăng nhập vào tài khoản của mình.</p><p>Có thể bạn muốn biết: <a href=\"https://nhanh.vn/cach-tao-su-kien-tren-fanpage-thu-hut-khach-hang-hieu-qua-n46172.html\"><i><strong>\"Cách tạo sự kiện trên Facebook thu hút khách hàng hiệu quả\"</strong></i></a></p><figure class=\"image\"><img src=\"https://cdn.nhanh.vn/cdn/store/26/artCT/58009/chinh_sach_bao_mat_thong_tin_khach_hang_2.jpg\" alt=\"Bảo mật thông tin\"></figure><p><i>Bảo mật thông tin khách hàng</i></p><p><strong>Trên trang web của The Face Shop họ có chia sẻ:</strong></p><h2><strong>1. Thu thập thông tin cá nhân</strong></h2><p>Chúng tôi sẽ thu thập nhiều thông tin khác nhau của bạn khi bạn muốn đặt hàng trên trang web.</p><p>Chúng tôi thu thập, lưu trữ và xử lý thông tin của bạn cho quá trình mua hàng, cho những thông báo sau này và để cung cấp dịch vụ. Chúng tôi không giới hạn thông tin cá nhân: danh hiệu, tên, giới tính, ngày sinh, email, địa chỉ, địa chỉ giao hàng, số điện thoại, fax, chi tiết thanh toán, chi tiết thanh toán bằng thẻ hoặc chi tiết tài khoản ngân hàng.</p><p>Chúng tôi sẽ dùng thông tin bạn đã cung cấp để xử lý đơn đặt hàng, cung cấp các dịch vụ và thông tin yêu cầu thông qua trang web và theo yêu cầu của bạn. Chúng tôi có thể chuyển tên và địa chỉ cho bên thứ ba để họ giao hàng cho bạn (ví dụ cho bên chuyển phát nhanh hoặc nhà cung cấp).</p><p>Các khoản thanh toán trực tuyến sẽ được xử lý bởi các đại lý mạng lưới quốc tế của chúng tôi. Bạn chỉ đưa cho chúng tôi hoặc đại lý hoặc trang web những thông tin chính xác, không gây nhầm lẫn và phải thông báo cho chúng tôi nếu có thay đổi.</p><p>Chi tiết đơn đặt hàng của bạn được chúng tôi lưu giữ nhưng vì lý do bảo mật nên chúng tôi không công khai trực tiếp được. Tuy nhiên, bạn có thể tiếp cận thông tin bằng cách đăng nhập tài khoản trên trang web. Tại đây, bạn sẽ thấy chi tiết đơn đặt hàng của mình, những sản phẩm đã nhận và những sản phẩm đã gửi và chi tiết email, ngân hàng và bản tin mà bạn đặt theo dõi dài hạn. Bạn cam kết bảo mật dữ liệu cá nhân và không được phép tiết lộ cho bên thứ ba. Chúng tôi không chịu bất kỳ trách nhiệm nào cho việc dùng sai mật khẩu nếu đây không phải lỗi của chúng tôi.</p><p>Đối tác thứ ba và liên kết</p><p>Chúng tôi có thể chuyển thông tin của bạn cho các công ty khác trong nhóm. Chúng tôi có thể chuyển thông tin của bạn cho các đại lý và nhà thầu phụ trong khuôn khổ quy định của chính sách bảo mật. Ví dụ: Chúng tôi sẽ nhờ bên thứ ba giao hàng, nhận tiền thanh toán, phân tích dữ liệu, tiếp thị và hỗ trợ dịch vụ khách hàng. Chúng tôi có thể trao đổi thông tin với bên thứ ba với mục đích chống gian lận và giảm rủi ro tín dụng. Trong khuôn khổ chính sách bảo mật, chúng tôi không bán hay tiết lộ dữ liệu cá nhân của bạn cho bên thứ ba mà không được đồng ý trước trừ khi điều này là cần thiết cho các điều khoản trong chính sách bảo mật hoặc chúng tôi được yêu cầu phải làm như vậy theo quy định của pháp luật.</p><p>THE FACE SHOP và các nhà cung cấp bên thứ ba, bao gồm Google, có thể sử dụng cookies của Google Analytics hoặc cookies của bên thứ ba (như DoubleClick) để <a href=\"https://nhanh.vn/3-ly-do-nen-su-dung-phan-mem-quan-ly-khach-hang-than-thiet-dung-bo-lo-n41522.html\"><i><strong>thu thập thông tin khách hàng</strong></i></a>, tối ưu hóa và phục vụ cho mục đích quảng cáo dựa trên lần truy cập trang web của người dùng trong quá khứ.</p><h2><strong>2. Sử dụng Cookie</strong></h2><p>Cookie là tập tin văn bản nhỏ có thể nhận dạng tên truy cập duy nhất từ máy tính của bạn đến máy chủ của chúng tôi khi bạn truy cập vào các trang nhất định trên trang web và sẽ được lưu bởi trình duyệt internet lên ổ cứng máy tính của bạn. Cookie được dùng để nhận dạng địa chỉ IP, lưu lại thời gian. Chúng tôi dùng cookie để tiện cho bạn vào trang web (ví dụ: ghi nhớ tên truy cập khi bạn muốn vào thay đổi lại giỏ mua hàng mà không cần phải nhập lại địa chỉ email của mình) và không đòi hỏi bất kỳ thông tin nào về bạn (ví dụ: mục tiêu quảng cáo). Trình duyệt của bạn có thể được thiết lập không sử dụng cookie nhưng điều này sẽ hạn chế quyền sử dụng của bạn trên trang web. Xin vui lòng chấp nhận cam kết của chúng tôi là cookie không bao gồm bất cứ chi tiết cá nhân riêng tư nào và an toàn với virus.</p><p>Trình duyệt này sử dụng Google Analytics, một dịch vụ phân tích trang web được cung cấp bởi Google, Inc. (“Google”). Google Analytics dùng cookie, là những tập tin văn bản đặt trong máy tính để giúp website phân tích người dùng vào trang web như thế nào. Thông tin được tổng hợp từ cookie sẽ được truyền tới và lưu bởi Google trên các máy chủ tại Hoa Kỳ. Google sẽ dùng thông tin này để đánh giá cách dùng trang web của bạn, lập báo cáo về các hoạt động trên trang web cho các nhà khai thác trang web và cung cấp các dịch vụ khác liên quan đến các hoạt động internet và cách dùng internet.</p><p>Google cũng có thể chuyển giao thông tin này cho bên thứ ba theo yêu cầu của pháp luật hoặc các bên thứ ba xử lý thông tin trên danh nghĩa của Google. Google sẽ không kết hợp địa chỉ IP của bạn với bất kỳ dữ liệu nào khác mà Google đang giữ. Bạn có thể từ chối dùng cookie bằng cách chọn các thiết lập thích hợp trên trình duyệt của mình, tuy nhiên lưu ý rằng điều này sẽ ngăn bạn sử dụng triệt để chức năng của trang web. Bằng cách sử dụng trang web này, bạn đã đồng ý cho Google xử lý dữ liệu về bạn theo cách thức và các mục đích nêu trên.</p><figure class=\"image\"><img src=\"https://cdn.nhanh.vn/cdn/store/26/artCT/58009/chinh_sach_bao_mat_thong_tin_khach_hang_5.jpg\" alt=\"Thông tin khách hàng\"></figure><p><i>Làm sao để bảo mật thông tin khách hàng?</i></p><h2><strong>3. Bảo mật</strong></h2><p>Chúng tôi có biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn truy cập trái phép hoặc trái pháp luật hoặc mất mát hoặc tiêu hủy hoặc thiệt hại cho thông tin của bạn. Khi thu thập dữ liệu trên trang web, chúng tôi thu thập chi tiết cá nhân của bạn trên máy chủ an toàn. Chúng tôi khuyên bạn rằng bạn không nên đưa thông tin chi tiết về việc thanh toán với bất kỳ ai bằng email, chúng tôi không chịu trách nhiệm về những mất mát bạn có thể gánh chịu trong việc trao đổi thông tin của bạn qua internet hoặc email.</p><p>Bạn tuyệt đối không sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. Nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm nhập vào dữ liệu của hệ thống trang web. Mọi vi phạm sẽ bị tước bỏ mọi quyền lợi cũng như sẽ bị truy tố trước pháp luật nếu cần thiết.</p><p>Mọi thông tin giao dịch sẽ được bảo mật nhưng trong trường hợp cơ quan pháp luật yêu cầu, chúng tôi sẽ buộc phải cung cấp những thông tin này cho các cơ quan pháp luật.</p><p>Các điều kiện, điều khoản và nội dung của trang web này được điều chỉnh bởi luật pháp Việt Nam và tòa án Việt Nam có thẩm quyền xem xét.</p><h2><strong>4. Quyền lợi khách hàng</strong></h2><p>Bạn có quyền yêu cầu truy cập vào dữ liệu cá nhân của mình, có quyền yêu cầu chúng tôi sửa lại những sai sót trong dữ liệu của bạn mà không mất phí. Bất cứ lúc nào bạn cũng có quyền yêu cầu chúng tôi ngưng sử dụng dữ liệu cá nhân của bạn cho mục đích tiếp thị.</p><p>Hiện nay nhu cầu mua sắm mỹ phẩm của chị em phụ nữ ngày càng lớn, việc <a href=\"https://nhanh.vn/top-4-website-thuong-mai-dien-tu-co-luong-truy-cap-khung-nhat-the-gioi-n56648.html\"><i><strong>truy cập vào các website để mua bán online cũng như các trang mạng điện tử khá nhiều</strong></i></a>. Vì vậy, Nhanh.vn muốn gửi đến bạn đọc bài viết này để các bạn hiểu rõ hơn về chính sách bảo mật của shop mỹ phẩm như thế nào và các bạn cũng nên tìm hiểu vì đó là các bạn đang tự bảo vệ chính mình.&nbsp;</p><p>Hiện nay, Nhanh.vn&nbsp;cung cấp một&nbsp;phần mềm quản lý bán hàng đa kênh&nbsp;giúp chủ cửa hàng và người bán hàng quản lý một cách xuyên suốt các hoạt động bán hàng của mình. Các đơn hàng từ website, Facebook, sàn thương mại điện tử sẽ được chuyển về phần mềm bán hàng của Nhanh rồi lựa chọn hãng vận chuyển để giao hàng đến cho khách hàng. Chính vì thế mà Nhanh đang là lựa chọn của rất nhiều shop trên địa bàn cả nước.</p><p>Vậy bạn còn chần chừ gi&nbsp;mà không nhanh tay liên lạc với chúng tôi để cuộc sống dễ dàng hơn mỗi ngày nhỉ? Hãy nắm bắt cơ hội của mình để giành lấy vị trí cao nhất trong lòng khách hàng nào!!! Nhanh.vn rất vui vì bạn đọc đã dành thời gian cho chúng mình, chúc bạn thành công trong cuộc sống.</p>', 4, '2021-06-09 01:33:15', 'image/chinh_sach_bao_mat_thong_tin_khach_hang_2.jpg', 2, 1),
(26, 8, 'Làm Sao Để Đặt Hàng Trên Trang Web Của RoyalSanest', 'Làm Sao Để Đặt Hàng Trên Trang Web Của RoyalSanest', '<p>Để mua hàng trên trang web của Công ty Cổ phần Yến sào Hoàng Yến bạn thực hiện theo các bước sau :</p><ol><li>Đăng kí tài khoản nếu chưa có.</li></ol><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/dang-ky.png\"><figcaption>Giao diện đăng ký tài khoản</figcaption></figure><p>2. Nếu đã có tài khoản đăng nhập, bạn đăng nhập vào website và chọn mục Sản phẩm</p><p>&nbsp;</p><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/sp-them-gio.png\"></figure><p>3. Thêm sản phẩm vào giỏ hàng bằng cách di chuyển chuột đến sản phẩm và ấn vào hình chiếc giỏ bên phải</p><p>4. Kiểm tra giỏ hàng</p><ul><li>Đầu tiên bạn chọn Giỏ hàng ở thanh công cụ, ở giao diện này bạn có thể thấy thông tin về các sản phẩm trong giỏ hàng.</li><li>Bạn có thể lựa chọn tiếp tục mua sắm ở dưới màn hình để quay trở lại trang sản phẩm</li><li>Lựa chọn thanh toán để đi đến bước cuối cùng</li></ul><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/gio-hang0001.png\"></figure><p>4. Thanh toán</p><ul><li>Điền đầy đủ thông tin vào mục thông tin khách hàng</li></ul><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/thanh-toan0001.png\"></figure><ul><li>Lựa chọn hình thức thanh toán phù hợp</li></ul><figure class=\"image\"><img src=\"http://localhost:3002/image/bai-viet/pttt0001.png\"></figure><ul><li>Chọn Đặt Hàng để hoàn tất.</li></ul>', 4, '2021-06-09 02:23:07', 'image/order.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_category`
--

CREATE TABLE `post_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `isActivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `post_category`
--

INSERT INTO `post_category` (`id`, `name`, `isActivated`) VALUES
(1, 'Giới thiệu', 1),
(2, 'Cẩm nang', 1),
(3, 'Tin tức', 1),
(4, 'Hướng dẫn và chính sách', 1),
(6, 'Khuyến mại', 1),
(7, 'Giảm giá', 1),
(8, 'Khuyến mại 2', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `typeProductId` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `description` text NOT NULL,
  `retailPrice` decimal(18,2) NOT NULL,
  `max` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `vendorId` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `typeProductId`, `view`, `description`, `retailPrice`, `max`, `min`, `vendorId`, `image`) VALUES
(1, 'TỔ YẾN TINH CHẾ LOẠI 1', 3, 16, '<p><strong>Tên sản phẩm: Tổ Yến Tinh Chế Loại 1</strong><br><br>Tổ Yến Tinh Chế Loại 1 là dòng Yến sào Thượng Hạng hay còn gọi là tổ yến cao cấp Khánh Hoà còn độ nguyên chất ~98% và là dòng sản phẩm có chất lượng tốt của Yến sào Hoàng Yến. Do hạn chế tối đa việc tiếp xúc với nhiều nước, ít thông qua đúc khuôn phục dựng, yến sào rút lông gần như giữ được hình dạng ban sơ của tổ yến, sạch lông, màu trắng ngà.<br><br>Ưu đãi hấp dẫn từ Yến sào Hoàng Yến: mua 100g yến sào Thượng Hạng, tặng kèm 1 hộp đường phèn + 1 hộp táo đỏ, hạt chia + 1 Thố chưng yến + 1 Yến sào Hoàng Yến VIP CARD.</p><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><strong>THÔNG TIN CHI TIẾT SẢN PHẨM</strong></td></tr><tr><td><strong>Phân nhóm</strong></td><td><strong>Tổ Yến Tinh Chế Loại 1</strong>&nbsp;– Yến sào Thượng Hạng Khánh Hoà</td></tr><tr><td><strong>Phân loại</strong></td><td>Thường</td></tr><tr><td><strong>Trọng lượng</strong></td><td>100gr (10 – 14 miếng/hộp)</td></tr><tr><td><strong>Bộ sản phẩm</strong></td><td><ul><li>100gr yến rút lông.</li><li>1 hộp đường phèn.</li><li>1 hộp táo đỏ, hạt chia</li><li>1 thố chưng yến.</li><li>1&nbsp;Yến sào Hoàng Yến&nbsp;Gold VIP CARD</li></ul></td></tr><tr><td><strong>Đặc trưng</strong></td><td>Sạch lông, màu trắng ngà nguyên thủy, 2 mặt sợi,&nbsp;liên kết bề mặt sợi có khe hở.<br>Tỷ lệ nguyên vẹn hình dáng ban sơ 50%.<br>Hình võng – cánh sen.</td></tr><tr><td><strong>Mùi</strong></td><td>Tanh nhẹ đặc trưng (mùi biển).</td></tr><tr><td><strong>Bảo quản</strong></td><td>3 năm theo chỉ định bảo quản</td></tr></tbody></table></figure>', 3900000.00, 3000, 5, 1, 'image/to-yen-tinh-che-loai-1_.jpg'),
(4, 'CHÂN YẾN CAO CẤP', 3, 7, '<p><strong>Chân yến cao cấp</strong></p><p>Được sàng lọc từ những tổ yến già tổ nhưng không còn nguyên vẹn, chân yến cao cấp của Hoàng Yến với chất lượng cao, đã được sơ chế sạch sẽ, sấy khô và đóng hộp, tiện dụng cho thượng khách.</p><p>Để so sánh chất lượng của Yến tinh chế và Chân yến thì Chân yến không hề kém cạnh - với những dưỡng chất đầy đủ như yến tinh chế nhưng giá thành lại mềm, hiệu quả sử dụng cũng rất tốt nên được các gia đình lựa chọn mua bởi tính kinh tế, và cảm giác ngon miệng khi ăn: giòn sật, đậm vị.</p><p>Chân yến thường có mùi tanh nồng hơn so với phần bụng yến, một số khách hàng khi chưng chân yến hay cho thêm lát gừng tươi để món ăn hấp dẫn hơn, nhưng một số khách hàng lại coi mùi nồng tanh nhẹ đặc trưng của Yến sào là một điều thú vị, vì thế chân yến của Hoàng Yến có phản hồi rất tốt từ khách.</p><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 1000000.00, 50, 20, 8, 'image/YẾN TINH CHẾ VIP 3.jpg'),
(10, 'TỔ YẾN TINH CHẾ SƠ MƯỚP', 3, 2, '<p><strong>Tên sản phẩm:</strong>&nbsp;Tổ Yến Tinh Chế Sơ Mướp</p><p><strong>Đặc điểm:</strong><br>&nbsp;</p><p><i><strong>Tổ Yến&nbsp;Tinh Chế Sơ Mướp&nbsp;</strong></i>&nbsp;là dòng Yến sào chất lượng tốt trong hệ thống tổ yến cao cấp Khánh Hoà với độ nguyên chất ~98% và là dòng sản phẩm có chất lượng tốt của&nbsp;<i><strong>Yến sào Hoàng Yến</strong></i>. Do hạn chế tối đa việc tiếp xúc với nhiều nước, ít thông qua đúc khuôn phục dựng, yến sào rút lông gần như giữ được hình dạng ban sơ của tổ yến, sạch lông, màu trắng ngà.</p><p>Ưu đãi hấp dẫn từ&nbsp;<i><strong>Yến sào Hoàng Yến</strong></i>: mua&nbsp;<strong>100g yến sào Thượng Hạng</strong>, tặng kèm&nbsp;<strong>1 hộp đường phèn + 1 hộp táo đỏ, hạt chia + 1 Thố chưng yến + 1&nbsp;</strong><i><strong>Yến sào Hoàng Yến&nbsp;</strong></i><strong>VIP CARD.</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><strong>THÔNG TIN CHI TIẾT SẢN PHẨM</strong></td></tr><tr><td><strong>Phân nhóm</strong></td><td>Tổ Yến Rút Lông – Yến sào Thượng Hạng Khánh Hoà</td></tr><tr><td><strong>Phân loại</strong></td><td>Thường</td></tr><tr><td><strong>Trọng lượng</strong></td><td>100gr (10 – 14 miếng/hộp)</td></tr><tr><td><strong>Bộ sản phẩm</strong></td><td><ul><li>100gr yến rút lông.</li><li>1 hộp đường phèn.</li><li>1 hộp táo đỏ, hạt chia</li><li>1 thố chưng yến.</li><li>1&nbsp;<i><strong>Yến sào Hoàng Yến</strong></i>&nbsp;Gold VIP CARD</li></ul></td></tr><tr><td><strong>Đặc trưng</strong></td><td>Sạch lông, màu trắng ngà nguyên thủy, 2 mặt sợi,&nbsp;liên kết bề mặt sợi có khe hở.<br>Tỷ lệ nguyên vẹn hình dáng ban sơ 50%.<br>Hình võng – cánh sen.</td></tr><tr><td><strong>Mùi</strong></td><td>Tanh nhẹ đặc trưng (mùi biển).</td></tr><tr><td><strong>Bảo quản</strong></td><td>3 năm theo chỉ định bảo quản</td></tr></tbody></table></figure>', 1500000.00, 150, 10, 6, 'image/to-yen-tinh-che-so-muop.jpg'),
(11, 'YẾN ĐẢO THÔ THƯỢNG HẠNG', 3, 1, '<p><strong>Yến đảo thô Thượng hạng</strong></p><p>Những tổ yến đảo được khai thác từ thiên nhiên, Hoàng Yến chọn lọc và lựa chọn ra những tai yến đẹp, dáng tai A, dầy mình, già sợi, sạch sẽ ít phân rêu rồi sấy khô đóng hộp. Quý khách khi mua tổ yến đảo thô Thượng hạng của Hoàng Yến sẽ được tặng thêm nhíp nhổ lông yến và bí quyết nhặt lông yến sao cho đảm bảo chất lượng tốt nhất.</p><p>Với những khách hàng không có thời gian nhặt lông yến nhưng lại muốn sử dụng Yến đảo thô Thượng hạng của Hoàng Yến thì Hoàng Yến cung cấp cả dịch vụ nhặt lông yến cho thượng khách, chất lượng của yến vẫn được giữ nguyên dưỡng chất, đóng túi để tiện cho khách hàng chế biến.</p><p>Yến đảo thô Thượng hạng của Hoàng Yến sau khi được nhặt sạch sẽ bảo quản được 06 tháng trong ngăn đá tủ lạnh đủ tiêu chuẩn.</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 1500000.00, 150, 10, 3, 'image/yen-dao-tho.jpg'),
(12, 'TỔ YẾN TINH CHẾ HẢO HẠNG', 3, 1, '<p><strong>Đặc điểm:</strong><br>&nbsp;</p><p><i><strong>Tổ Yến Tinh Chế Hảo Hạng&nbsp;</strong></i>&nbsp;là dòng Yến sào Thượng Hạng hay còn gọi là tổ yến cao cấp Khánh Hoà còn độ nguyên chất ~98% và là dòng sản phẩm có chất lượng tốt của <i><strong>Yến sào Hoàng Yến</strong></i>. Do hạn chế tối đa việc tiếp xúc với nhiều nước, ít thông qua đúc khuôn phục dựng, yến sào rút lông gần như giữ được hình dạng ban sơ của tổ yến, sạch lông, màu trắng ngà.</p><p>Ưu đãi hấp dẫn từ&nbsp;<i><strong>Yến sào Hoàng Yến</strong></i>: mua&nbsp;<strong>100g yến sào Thượng Hạng</strong>, tặng kèm&nbsp;<strong>1 hộp đường phèn + 1 hộp táo đỏ, hạt chia + 1 Thố chưng yến + 1&nbsp;</strong><i><strong>Nhà Yến Việt +&nbsp;</strong></i><strong>VIP CARD</strong>.</p><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><strong>THÔNG TIN CHI TIẾT SẢN PHẨM</strong></td></tr><tr><td><strong>Phân nhóm</strong></td><td>Tổ Yến Tinh Chế Hảo Hạng – Yến sào Thượng Hạng Khánh Hoà</td></tr><tr><td><strong>Phân loại</strong></td><td>Thường</td></tr><tr><td><strong>Trọng lượng</strong></td><td>100gr (10 – 14 miếng/hộp)</td></tr><tr><td><strong>Bộ sản phẩm</strong></td><td><ul><li>100gr yến rút lông.</li><li>1 hộp đường phèn.</li><li>1 hộp táo đỏ, hạt chia</li><li>1 thố chưng yến.</li><li>1 <i><strong>Yến sào Hoàng Yến</strong></i>&nbsp;Gold VIP CARD</li></ul></td></tr><tr><td><strong>Đặc trưng</strong></td><td>Sạch lông, màu trắng ngà nguyên thủy, 2 mặt sợi,&nbsp;liên kết bề mặt sợi có khe hở.<br>Tỷ lệ nguyên vẹn hình dáng ban sơ 50%.<br>Hình võng – cánh sen.</td></tr><tr><td><strong>Mùi</strong></td><td>Tanh nhẹ đặc trưng (mùi biển).</td></tr><tr><td><strong>Bảo quản</strong></td><td>3 năm theo chỉ định bảo quản</td></tr></tbody></table></figure>', 3500000.00, 300, 120, 5, 'image/to-yen-tinh-che-hao-hang.jpg'),
(13, 'YẾN CHƯNG HŨ', 3, 1, '<p><strong>Tên sản phẩm:</strong> Yến chưng hũ</p><p>• <a href=\"https://royalsanest.com/\"><strong>YẾN SÀO HOÀNG YẾN</strong></a> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohoangyen.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến Sào Hoàng Yến - Hải Phòng</strong></a></p>', 500000.00, 6, 1, 3, 'image/yen-chung.jpg'),
(16, 'TỔ YẾN TINH CHẾ LOẠI 2', 3, 1, '<p><strong>Đặc điểm:</strong></p><p>&nbsp;</p><p><i><strong>Tổ Yến Tinh Chế Loại 2 100gr</strong>&nbsp;</i>của &nbsp;Yến sào Hoàng Yến&nbsp;<strong> </strong>&nbsp;là tổ yến làm sạch hay còn gọi là yến sào tinh chế đã được xử lý sạch, sợi phồng, trắng, có độ bóng ánh kim, hình cánh sen đẹp mắt. Tỉ lệ sợi gãy/đứt và sợi nguyên: 50%-50% bề mặt, phần bụng sợi vụn. Tiện lợi để sử dụng ngay, tiết kiệm thời gian và lượng hao hụt trong quá trình xử lý.</p><p>Ưu đãi hấp dẫn từ&nbsp; Yến sào Hoàng Yến : mua&nbsp;<strong>100g&nbsp;</strong><i><strong>Tổ Yến Tinh Chế Loại 2</strong></i>, tặng kèm&nbsp;<strong>1 hộp đường phèn + 1 hộp táo đỏ, hạt chia + 1 Thố chưng yến + 1&nbsp;</strong> Yến sào Hoàng Yến&nbsp;<i><strong> </strong></i><strong>VIP CARD</strong>.</p><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><strong>THÔNG TIN CHI TIẾT SẢN PHẨM</strong></td></tr><tr><td><strong>Phân nhóm</strong></td><td>Tổ yến làm sạch</td></tr><tr><td><strong>Phân loại</strong></td><td>Tổ Yến Tinh Chế Loại 2&nbsp;</td></tr><tr><td><strong>Trọng lượng</strong></td><td>100gr (10 miếng/hộp)</td></tr><tr><td><strong>Bộ sản phẩm</strong></td><td><ul><li>100gr yến làm sạch tiêu chuẩn thường</li><li>1 hộp đường phèn</li><li>1 hộp mix: sen, kỉ tử, hại chia, táo đỏ</li><li>1 hũ chưng yến</li></ul></td></tr><tr><td><strong>Đặc trưng</strong></td><td>Sạch lông, màu trắng nguyên thủy,&nbsp;sợi phồng.<br>Tỷ lệ sợi gãy/đứt 50% và sợi nguyên 50%.<br>Hình dáng võng – cánh sen.</td></tr><tr><td><strong>Mùi</strong></td><td>Tanh nhẹ đặc trưng (mùi biển).</td></tr><tr><td><strong>Bảo quản</strong></td><td>3 năm theo chỉ định bảo quản</td></tr></tbody></table></figure></td></tr></tbody></table></figure>', 7000000.00, 10000, 12, 5, 'image/to-yen-tinh-che-loai-2.jpg'),
(17, 'YẾN ĐẢO', 7, 0, '<p>Tên sản phẩm : Yến đảo</p><p>&nbsp;</p><p>Giới thiệu sản phẩm : Yến đảo sinh sống trong các hang động với điều kiện thiên nhiên phù hợp, mát, ẩm độ cao. Yến nhà được nuôi trong nhà chuyên dùng để nuôi yến; được lắp thiết bị dẫn dụ và thiết bị tạo môi trường nhiệt độ và độ ẩm gần giống như trong các hang động ở đảo, vì vậy Yến có chất lượng tốt giống như môi trường tự nhiên mà lại không gây tổn hại đến loài yến tự nhiên.</p><p>&nbsp;</p><p>Yến đảo của Hoàng Yến sau khi được nhặt sạch sẽ bảo quản được 06 tháng trong ngăn đá tủ lạnh đủ tiêu chuẩn.</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 750000.00, 5000, 10, 3, 'image/yen-dao-tho.jpg'),
(20, 'TỔ YẾN TINH CHẾ HẢO HẠNG', 3, 0, '<p><strong>Đặc điểm:</strong><br><br>&nbsp;</p><p><i><strong>Tổ Yến Tinh Chế Hảo Hạng&nbsp;</strong></i>&nbsp;là dòng Yến sào Thượng Hạng hay còn gọi là tổ yến cao cấp Khánh Hoà còn độ nguyên chất ~98% và là dòng sản phẩm có chất lượng tốt của <i><strong>Yến sào Hoàng Yến</strong></i>. Do hạn chế tối đa việc tiếp xúc với nhiều nước, ít thông qua đúc khuôn phục dựng, yến sào rút lông gần như giữ được hình dạng ban sơ của tổ yến, sạch lông, màu trắng ngà.</p><p>Ưu đãi hấp dẫn từ&nbsp;<i><strong>Yến sào Hoàng Yến</strong></i>: mua&nbsp;<strong>100g yến sào Thượng Hạng</strong>, tặng kèm&nbsp;<strong>1 hộp đường phèn + 1 hộp táo đỏ, hạt chia + 1 Thố chưng yến + 1&nbsp;</strong><i><strong>Nhà Yến Việt +&nbsp;</strong></i><strong>VIP CARD</strong>.</p><figure class=\"table\"><table><tbody><tr><td colspan=\"2\"><strong>THÔNG TIN CHI TIẾT SẢN PHẨM</strong></td></tr><tr><td><strong>Phân nhóm</strong></td><td>Tổ Yến Tinh Chế Hảo Hạng – Yến sào Thượng Hạng Khánh Hoà</td></tr><tr><td><strong>Phân loại</strong></td><td>Thường</td></tr><tr><td><strong>Trọng lượng</strong></td><td>100gr (10 – 14 miếng/hộp)</td></tr><tr><td><strong>Bộ sản phẩm</strong></td><td><ul><li>100gr yến rút lông.</li><li>1 hộp đường phèn.</li><li>1 hộp táo đỏ, hạt chia</li><li>1 thố chưng yến.</li><li>1 <i><strong>Yến sào Hoàng Yến</strong></i>&nbsp;Gold VIP CARD</li></ul></td></tr><tr><td><strong>Đặc trưng</strong></td><td>Sạch lông, màu trắng ngà nguyên thủy, 2 mặt sợi,&nbsp;liên kết bề mặt sợi có khe hở.<br>Tỷ lệ nguyên vẹn hình dáng ban sơ 50%.<br>Hình võng – cánh sen.</td></tr><tr><td><strong>Mùi</strong></td><td>Tanh nhẹ đặc trưng (mùi biển).</td></tr><tr><td><strong>Bảo quản</strong></td><td>3 năm theo chỉ định bảo quản</td></tr></tbody></table></figure>', 4000000.00, 4, 1, 3, 'image/to-yen-tinh-che-hao-hang.jpg'),
(23, 'Đông Trùng Hạ Thảo Tây Tạng Thượng Hạng', 2, 0, '<p><strong>Đông trùng hạ thảo Tây Tạng thượng hạng</strong></p><p>&nbsp;</p><ul><li>Tây Tạng có thể được coi là “quê hương” của đông trùng hạ thảo. Bởi loại dược liệu này được tìm thấy chủ yếu tại vùng núi cao trên 4000m. Với khí hậu thích hợp để loài nấm Cordyceps Sinensis sinh sôi, đông trùng hạ thảo Tây Tạng Trung Quốc phát triển vô cùng tốt và mang hàm lượng dưỡng chất cực kỳ lớn.Dược tính và công dụng mà loại dược liệu quý hiếm này đem lại được đánh giá là vượt trội hơn đông trùng hạ thảo nhân tạo.</li><li>Những tác dụng vượt trội của ĐTHT Tây Tạng:</li><li>Giúp nâng cao sức khỏe: Đông trùng hạ thảo giúp kích thích sản sinh oxy tới các tế bào, giúp cơ thể khỏe mạnh hơn, giảm tình trạng suy nhược, mệt mỏi.</li><li>Đông trùng hạ thảo Tây Tạng tác dụng đến hệ miễn dịch: Giúp tăng cường khả năng miễn dịch, kích thích phản ứng kháng viêm và chống lại vi khuẩn, virus hiệu quả hơn.</li><li>Cân bằng và điều hòa nội tiết tố: Nhờ khả năng này mà đông trùng hạ thảo giúp cải thiện những vấn đề do rối loạn nội tiết gây ra như nóng trong, rụng tóc, mệt mỏi, căng thẳng, nổi mụn,… Đồng thời đẩy lùi tình trạng lão hóa, giúp làn da luôn tươi trẻ.</li><li>Cải thiện sức khỏe sinh lý: Đông trùng hạ thảo giúp tăng cường sinh lý, cải thiện tình trạng suy giảm ham muốn ở cả nam giới và nữ giới.</li><li>Hỗ trợ điều trị bệnh: Cải thiện chức năng gan, thận, giảm nguy cơ sỏi mật; khắc phục chứng lạnh tử cung, đẩy lùi bệnh tiểu đường, tăng huyết áp, ung thư,…</li><li>Phòng ngừa một số bệnh lý: Dùng đông trùng hạ thảo sẽ giúp bạn phòng tránh được các bệnh hô hấp như hen suyễn, viêm phổi, viêm phế quản. Đồng thời bảo vệ tim mạch, ngăn ngừa rối loạn nhịp tim, tăng huyết áp, xơ vữa động mạch,…</li><li>Cách sử dụng: Đông trùng hạ thảo có vị ngọt, nhẹ nhàng và không độc hại. Đây là một loại thuốc bổ dưỡng nổi tiếng. Nó thường được sử dụng bằng cách hầm trong món ăn và có tác dụng dưỡng sinh. Đông Trùng Hạ Thảo có thể ăn trực tiếp, hãm trà, chưng yến,ngâm mật ong, cho vào các món ăn…</li></ul><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 3500000.00, 15, 1, 8, 'image/dong-trung-ha-thao1.jpg'),
(24, 'ĐÔNG TRÙNG HẠ THẢO NGUYÊN CON THỂ KHÔ', 2, 1, '<p><strong>Đông trùng hạ thảo nguyên con thể khô</strong></p><ul><li>Nhộng trùng thảo còn có tên khoa học là Cordyceps militaris là một loại dược liệu quý thường, là sự cộng sinh của hai loài ấu trùng nhộng tằm và nấm Cordyceps. Chúng mọc tua tủa trên khắp các bộ phận của vật chủ, có thân màu vàng cam hoặc hơi ngả hồng, đầu nấm dạng chuỳ phình to. Hiện nay tại Việt Nam đã có những cơ sở đủ điều kiện nuôi cấy, thu hoạch, được kiểm định đủ điều kiện về tính dược lý của sản phẩm và đưa ra thị trường.</li><li>Sử dụng Nhộng đông trùng sấy khô giúp cho ổn định huyết áp, giảm mỡ máu, giảm men gan. Ngoài ra còn hỗ trợ rất tốt cho những bệnh nhân có vấn đề tim mạch, bệnh thận, bệnh phổi, bệnh đường hô hấp. Duy trì sử dụng nhộng trùng thảo hàng ngày còn giúp ngăn ngừa các bệnh tiểu đường, ngăn ngừa tai biến, hỗ trợ điều trị các bệnh nan y, giúp trẻ hoá cơ thể, tăng cường khả năng sinh lý của cả nam và nữ.</li><li>Cách sử dụng: hãm trà, chưng yến, nấu cháo, nấu suop, tần gà ,….</li><li>Bảo quản nơi thoáng mát, tránh ánh sáng trực tiếp.</li></ul><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 6000000.00, 18, 1, 5, 'image/dong-trung-ha-thao-2.jpg'),
(25, 'ĐÔNG TRÙNG HẠ THẢO NGUYÊN CON TƯƠI', 2, 0, '<p>Đông trùng hạ thảo nguyên con tươi&nbsp;</p><p>Đây là dạng Đông trùng hạ thảo nguyên con được giữ nguyên khi nó phát triển ký sinh trên thân chủ là nhộng tằm. Hoàng Yến cam kết chất lượng : phần nhộng còn căng đầy, nguyên vẹn, phần nấm phía trên có màu vàng cam, sợi nguyên, không đứt gãy, chất lượng tốt, giữ nguyên dược chất quý giá là chất Cordycepin và Adenosine.&nbsp;</p><p>Đông trùng hạ thảo nguyên con tươi rất tốt cho những người bị suy nhược cơ thể, người chơi thể thao, người sau phẫu thuật, người sau hóa trị xạ trị, người có vấn đề về tim mạch – huyết áp, người đang điều trị bệnh mạn tính hoặc người cần tăng cường sức khỏe.&nbsp;</p><p>Đông trùng hạ thảo nguyên con tươi có thể dùng :&nbsp;</p><ul><li>Pha trà: hãm với nước sôi trong bình kín, nếu ngâm 3-5 tiếng thì rất tốt, uống nước và ăn cả xác. *&nbsp;</li><li>Ngâm mật ong: 100gr đông trùng hạ thảo nguyên con tươi + 300ml mật ong nguyên chất, mỗi ngày dùng 10ml và 3 con nhộng.&nbsp;</li><li>Ngâm rượu : 100gr đông trùng hạ thảo nguyên con tươi + 4 lít rượu, uống mỗi lần 2 chén nhỏ. Nấu cháo:&nbsp;</li><li>Hầm với cốt cháo khoảng 1 tiếng để chiết xuất hoàn toàn, sau đó sử dụng.&nbsp;</li><li>Các món hầm: cho vào nồi ninh hầm khoảng 1 tiếng, sau đó sử dụng Hoàng Yến khuyên quý khách nên bảo quản nơi thoáng mát, tránh ánh sáng.</li></ul><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: <a href=\"http://www.royalsanest.com/\"><strong>www.royalsanest.com</strong></a></p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 5000000.00, 20, 1, 5, 'image/dong-trung-ha-thao-tuoi.jpg'),
(26, 'ĐÔNG TRÙNG HẠ THẢO QUẢ THỂ TƯƠI', 2, 0, '<p><strong>Đông trùng hạ thảo quả thể tươi</strong></p><ul><li>Đông trùng hạ thảo có giá trị ở tính chất dược lý, đó là chất Cordycepin và Adenosine. Với phương pháp hiện đại ngày nay đã có dạng đông trùng hạ thảo được trực tiếp nuôi cấy trên sinh khối trong môi trường nhân tạo đảm bảo đủ 2 dưỡng chất trên.</li></ul><p>Hoàng Yến đã hợp tác với cơ sở đủ điều kiện để đưa ra thị trường những hộp Đông trùng hạ thảo quả thể tươi được đóng trong hộp , có màu vàng cam, dạng sinh khối, có tác dụng rất tốt cho những người bị suy nhược cơ thể, người chơi thể thao, người sau phẫu thuật, người sau hóa trị xạ trị, người có vấn đề về tim mạch – huyết áp, người đang điều trị bệnh mạn tính hoặc người cần tăng cường sức khỏe.</p><ul><li>Khi quý khách mua Đông trùng hạ thảo quả thể tươi của Hoàng Yến sẽ được tặng cẩm nang sử dụng với nhiều cách chế biến dễ thực hiện mà đảm bảo được dưỡng chất:</li><li>Ăn trực tiếp: dùng nước ấm rửa sơ phần nấm phía trên, loại bỏ đế ở dưới, nhai sống và nuốt cả xác.</li><li>&nbsp;Hãm trà: Hãm 5-7 sợi đông trùng với nước sôi, sau 5-7 phút là dùng được, uống thay trà trong ngày, ăn cả xác nấm.</li><li>Chế biến món ăn: các loại thịt sau khi hầm nhừ đã chín, thả sợi nấm đông trùng vào, đun sôi 2-3 phút sau đó bắc ra thưởng thức, lúc này vị nấm giòn bùi sật, thơm ngậy.&nbsp; Có thể dùng cho cả món canh</li><li>Nấu cháo: sau khi cháo chín, bỏ sợi đông trùng vào đun thêm 3-4 phút nữa rồi sử dụng</li><li>Ngâm rượu : 5 sinh khối đông trùng (giữ nguyên đế) ngâm với 2 lít rượu</li><li>Lưu ý: Hoàng Yến khuyên quý khách nên sử dụng Đông trùng hạ thảo quả thể tươi trong vòng 2 tuần , bảo quản trong ngăn mát tủ lạnh. Sử dụng không hết nên đậy nắp và lại bảo quản trong ngăn mát tủ lạnh.</li><li>Không nên sử dụng cho người mang thai 3 tháng đầu, trẻ em thì dùng không quá 1gr/ngày và 1 tuần dùng 2 lần.</li></ul><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 6500000.00, 18, 1, 8, 'image/dong-trung-ha-thao-tuoi03.jpg'),
(27, 'HỒNG YẾN TINH CHẾ THƯỢNG HẠNG', 5, 0, '<p><strong>Hồng Yến tinh chế Thượng hạng</strong></p><p>Với sản lượng chỉ chiếm 1% tổng sản lượng yến sào thì Hồng Yến là mặt hàng quý giá và là món quà biếu mang giá trị cao cấp. Hồng yến của Hoàng Yến có màu cam hồng nhẹ bởi màu hoàn toàn tự nhiên. Hoàng yến cam kết không ủ màu cho Hồng yến, vì thế giữ được chất lượng cao nhất của dòng yến quý giá này.</p><p>&nbsp;</p><p>Hồng yến tinh chế Thượng hạng của Hoàng yến có những vi chất rất quý giá, chứa hàm lượng Protein cao, nhiều loại acid amin, khoáng chất cần thiết mà con người không tự tổng hợp được, và các thành phần hữu ích như: glycol, aspartic acid, serine, tyrosine, phenylalanine, valine, arginine, leucine….</p><p>&nbsp;</p><p>Khi mua Hồng yến tinh chế thượng hạng tại Hoàng Yến, quý khách được tặng những món quà tặng như : thố/hũ thủy tinh chưng yến, táo đỏ, hạt chia, kỷ tử, long nhãn, đường phèn…</p><p>Hoàng Yến tự hào là nơi cung cấp những tổ Hồng yến tinh chế Thượng hạng cao cấp và uy tín,&nbsp; là địa chỉ tin cậy của rất nhiều khách hàng.</p><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 1500000.00, 17, 1, 5, 'image/hong-yen-tinh-che-thuong-hang.jpg'),
(28, 'YẾN ĐẢO THÔ THƯỢNG HẠNG LOẠI 1', 3, 1, '<p><strong>Yến đảo thô Thượng hạng</strong></p><p>Những tổ yến đảo được khai thác từ thiên nhiên, Hoàng Yến chọn lọc và lựa chọn ra những tai yến đẹp, dáng tai A, dầy mình, già sợi, sạch sẽ ít phân rêu rồi sấy khô đóng hộp. Quý khách khi mua tổ yến đảo thô Thượng hạng của Hoàng Yến sẽ được tặng thêm nhíp nhổ lông yến và bí quyết nhặt lông yến sao cho đảm bảo chất lượng tốt nhất.</p><p>Với những khách hàng không có thời gian nhặt lông yến nhưng lại muốn sử dụng Yến đảo thô Thượng hạng của Hoàng Yến thì Hoàng Yến cung cấp cả dịch vụ nhặt lông yến cho thượng khách, chất lượng của yến vẫn được giữ nguyên dưỡng chất, đóng túi để tiện cho khách hàng chế biến.</p><p>Yến đảo thô Thượng hạng của Hoàng Yến sau khi được nhặt sạch sẽ bảo quản được 06 tháng trong ngăn đá tủ lạnh đủ tiêu chuẩn.</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 750000.00, 78, 1, 8, 'image/yen-dao-tho-thuong-hang.jpg'),
(29, 'KỲ TỬ ĐỎ', 11, 2, '<p><strong>Kỷ tử đỏ</strong></p><ul><li>Kỷ tử đỏ Ninh Hạ được Hoàng Yến chọn lọc từ những nhà cung cấp uy tín chất lượng tại Trung Quốc và xách tay về Việt Nam</li><li>Kỷ tử đỏ hay được gọi là Câu kỷ tử - thuộc họ quả mọng, thường thấy trong các bài thuống Đông Y với rất nhiều công dụng tốt cho sức khoẻ.</li><li>Kỷ tử đỏ có vị đắng xen lẫn một chút vị chua nhẹ nhưng kèm cảm giác ngọt ở hậu vị. Đặc biệt các chuyên gia dinh dưỡng đánh giá cao kỷ tử sấy khô bởi hàm lượng dinh dưỡng cô đặc ở mức cao.</li><li>Kỷ tử đỏ là một trong những loại quả chứa nhiều vitamin và khoáng chất: sắt, kẽm,&nbsp; chất xơ, vitamin C, vitamin A, chất chống oxy hoá, 8 axit amin thiết yếu.</li><li>Công dụng:</li><li>Tăng cường thị lực</li><li>Giúp giảm cân</li><li>Cải thiện khả năng tình dục</li><li>Chống trầm cảm</li><li>Thải độc gan, tăng cường hệ miễn dịch, chữa suy nhược cơ thể</li><li>Hỗ trợ giảm đau, đặc biệt là đau khớp</li><li>Làm đẹp da, giúp tóc nhanh dài</li><li>Cải thiện sức khoẻ của phổi</li><li>Cải thiện huyết áp, tốt cho hệ tim mạch</li><li>Cải thiện bệnh tiểu đường</li><li>Hỗ trợ điều trị ung thư dạ dày</li><li>Cách sử dụng : Dùng để hãm trà, chưng yến, nấu chè dưỡng nhan, chế biến các món tần, hầm…</li><li>Hãm trà : Hoàng Yến khuyên quý khách hãy Hãm trà với nhiệt độ khoảng 70 độ C để giữ được những dưỡng chất của Kỷ tử, dùng khi còn ấm, có thể cho thêm mật ong để dễ uống hơn.</li><li>Chưng yến: Những hũ yến chưng cho thêm Kỷ tử không những giúp hũ yến thơm ngon bổ dưỡng hơn mà còn rất đẹp mắt.</li><li>Ngâm rượu: 1 kg kỷ tử + 5-7 lít rượu ngâm vào bình thủy tinh hoặc sứ, ngâm trong vòng 100 ngày là uống được. Mỗi lần uống 15-20ml rượu kỷ tử.</li><li>Cho vào chế biến các món ăn như tần, hầm, nấu chè…</li><li>Thận trọng khi dùng cho phụ nữ có thai và cho con bú</li><li>Bảo quản nơi khô ráo thoáng mát.</li></ul><p>&nbsp;</p><p>&nbsp; <i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 500000.00, 17, 2, 6, 'image/ky-tu-do.jpg'),
(30, 'HẮC KỶ TỬ TÂY TẠNG', 11, 1, '<p><strong>Hắc kỷ tử Tây Tạng</strong></p><p>Tại Hoàng Yến, chúng tôi đã lựa chọn Hắc Kỷ tử Tây Tạng chất lượng nhất ở vùng đất Ninh Hạ để đưa đến tay khách hàng</p><ul><li>Hắc kỷ tử Tây Tạng sở hữu lượng OPCs (các hợp chất thực vật phức tạp) cực lớn , nồng độ cao nhất được được phát hiện trong tự nhiên hiện nay. Chất này được tìm thấy trong trái cây, rau và vỏ cây nhất định có lợi ích dinh dưỡng cho cơ thể người</li></ul><p>Các nghiên cứu cho thấy mức độ oxy hoá của OPCs mạnh gấp 20 lần so với vitamin C và mạnh gấp 50 lần so với vitamin E. Người dân Tây Tạng coi Hắc kỷ tử là món quà trời ban bởi tác dụng chống oxy hoá, kháng ung thư cực mạnh.</p><ul><li>&nbsp;Công dụng:</li><li>Hắc kỷ tử còn rất tốt cho phụ nữ: nhuận sắc, ngăn ngừa rối loạn kinh nguyệt, giảm nhẹ các triệu chứng tiền mãn kinh</li><li>Chống lão hoá, chữa trị bệnh bạc tóc sớm ở người trẻ tuổi</li><li>Làm giảm và sửa chữa các tế bào bị hư hại do hậu quả của sự viêm và sự oxy hoá do các gốc tự do gây ra.</li><li>Tăng cường hệ miễn dịch, bảo vệ cơ thể, gia tăng tuổi thọ</li><li>Giúp sáng mắt, bảo vệ và cải thiện thị lực, đặc biệt tốt cho những bệnh nhân đã làm phẫu thuật thay thuỷ tinh thể.</li><li>Ổn định huyết áp đối với người bị bệnh Cao huyết áp</li><li>Ngăn ngừa sự phát triển của tế bào ung thư</li><li>Giảm Cholesterol, ngăn ngừa bệnh tim</li><li>Điều chỉnh lượng máu trong cơ thể để tái tạo tế bào trong cơ thể và da</li><li>Ổn định chức năng tế bào não.</li><li>Cách sử dụng:</li><li>Hãm trà : tùy vào nguồn nước mà khi hãm trà Hắc kỷ tử sẽ cho ra màu khác nhau. Nếu pha với nước có tính kiềm (nước máy đun sôi) thì nó sẽ có màu xanh lam, còn nếu pha với nước có tính acid (nước khoáng đun sôi) thì màu lại trở thành tím hồng, điều này vô cùng thú vị và rất được chị em ưa thích. Hoàng Yến khuyên quý khách hãy Hãm trà với nhiệt độ khoảng 70 độ C để giữ được những dưỡng chất của Hắc Kỷ tử, dùng khi còn ấm, có thể cho thêm mật ong để dễ uống hơn.</li><li>Chưng yến</li><li>Ngâm rượu: 1 kg hắc kỷ tử + 5-7 lít rượu ngâm vào bình thủy tinh hoặc sứ, ngâm trong vòng 100 ngày là uống được. Mỗi lần uống 15-20ml rượu Hắc kỷ tử.</li><li>Cho vào chế biến các món ăn như tần, hầm, nấu chè…</li><li>Bảo quản nơi khô ráo thoáng mát.</li><li>Thận trọng khi dùng cho phụ nữ có thai và cho con bú</li></ul><p>&nbsp;</p><p>&nbsp; <i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến sào Hoàng Yến</strong></a></p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 850000.00, 85, 2, 5, 'image/hac-ky-tu.jpg'),
(31, 'GIỎ QUÀ CAO CẤP 01', 6, 0, '<p><strong>Tên sản phẩm:</strong>&nbsp;GIỎ&nbsp;QUÀ</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 6000000.00, 10, 1, 8, 'image/gio-qua-1.jpg'),
(32, 'HỘP QUÀ', 6, 0, '<p><strong>Tên sản phẩm:</strong> Hộp quà</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 7500000.00, 9, 1, 5, 'image/HOP-QUA-2.jpg'),
(33, 'HỘP QUÀ CAO CẤP 02', 6, 1, '<p><strong>Tên sản phẩm:</strong> Hộp quà</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến Sào Hoàng Yến - Hải Phòng</strong></a></p>', 2000000.00, 50, 1, 8, 'image/hop-qua-02.jpg'),
(34, 'HỘP QUÀ THƯỢNG HẠNG', 6, 0, '<p><strong>Tên sản phẩm:</strong> Hộp quà thượng hạng</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 2500000.00, 12, 1, 5, 'image/hop-qua-thuong-hang.jpg'),
(35, 'HỘP QUÀ CAO CẤP 03', 6, 0, '<p><strong>Tên sản phẩm:</strong> Hộp quà cao cấp 03</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 3200000.00, 14, 1, 8, 'image/hop-qua-03.jpg'),
(36, 'HỘP QUÀ CAO CẤP 01', 6, 0, '<p><strong>Tên sản phẩm:</strong>&nbsp;HỘP QUÀ&nbsp;</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz&nbsp;</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 800000.00, 80, 1, 1, 'image/hop-qua-01.jpg'),
(37, 'ĐÔNG TRÙNG TÚI NƯỚC HÀN QUỐC', 2, 0, '<p><strong>Đông trùng túi nước Hàn Quốc</strong></p><ul><li>Xuất xứ: Hàn Quốc</li><li>Quy cách : Hộp gỗ, 60 túi x 30ml</li><li>Công dụng:</li><li>Bổ thận, tráng dương, tăng cường sinh lý của nam và nữ</li><li>Bồi bổ cơ thể, chống suy nhược, dung cho người hay bị mệt mỏi, căng thẳng, người hay ốm vặt, người mới bị mổ hay mới ốm dậy.</li><li>Điều trị ho sổ mũi, ngạt mũi khá hiệu quả</li><li>Điều hoà huyết áp, ngăn ngừa xơ vữa động mạch, chống gan mỡ, hỗ trợ điều trị các bệnh hen suyễn</li><li>Giúp ăn ngon, ngủ tốt, điều trị chứng mất ngủ, khó ngủ</li><li>Tăng cường khả năng miễn dịch, thải độc gan thận. Những người hay sử dụng rượu bia hoặc thuốc tây nên dung Đông trùng hạ thảo dạng nước.</li><li>Làm chậm quá trình lão hoá, tăng cường lưu thông máu, giảm Cholesterol, chống xơ vữa động mạch</li><li>Cải thiện chức năng tiêu hoá, giúp chống bị đi ngoài hay táo bón</li><li>Đặc biệt Đông trùng hạ thảo giúp bệnh nhân điều trị tiểu đường rất hiệu quả.</li><li>Cách dùng đông trùng hạ thảo dạng túi: Ngày dùng từ 1- 2 gói, cắt túi rồi uống trực tiếp. Mùa đông có thể ngâm gói đông trùng vào nước ấm rồi cắt ra uống nhằm tránh viêm họng.</li></ul><p>&nbsp;<i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 1000000.00, 50, 1, 8, 'image/dtht-nuoc-han-quoc.jpg'),
(38, 'ĐÔNG TRÙNG HẠ THẢO VIÊN TAKICHI', 2, 0, '<p><strong>Đông trùng viên Takichi</strong></p><ul><li>Hoàng Yến là nơi cung cấp Đông trùng viên của Takichi với những Công dụng như sau:</li><li>Giúp cải thiện suy nhược cơ thể</li><li>Giúp giảm mệt mỏi, bồi bổ cơ thể</li><li>Giúp giảm các triệu chứng suy thận, suy yếu sinh lý, đau lưng nhức mỏi..</li><li>Giúp tăng cường hệ miễn dịch và tăng lưu lượng tuần hoàn máu.</li><li>Đối tượng sử dụng:</li><li>Người gầy yếu, mệt mỏi, suy nhược thể lực, suy nhược thần kinh</li><li>Người mới ốm dậy cần phục hồi sức khoẻ</li><li>Người cần bồi bổ sức khoẻ sau phẫu thuật</li><li>Người đang điều trị bệnh</li><li>Người cần tăng cường sức đề kháng, phòng ngừa bệnh tật</li><li>Người bị đau lưng, người bị ho lao</li><li>Người yếu chức năng thận, yếu sinh lý</li><li>Hướng dẫn sử dụng: Ngày 2 lần, mỗi lần 2 viên, dùng tốt nhất trước bữa ăn 15-30 phút</li><li>Thực phẩm này không phải là thuốc, không có tác dụng thay thế thuốc chữa bệnh</li></ul><p>&nbsp; <i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến sào Hoàng Yến</strong></a></p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 320000.00, 150, 10, 8, 'image/dong-trung-vien001.jpg'),
(39, 'NẤM LINH CHI CAO CẤP', 11, 0, '<p>Tên Sản Phẩm: Nấm Linh Chi Cao Cấp</p><p>• <a href=\"http://www.royalsanest.com/\"><strong>YẾN SÀO HOÀNG YẾN</strong></a> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến Sào Hoàng Yến - Hải Phòng</strong></a></p>', 500000.00, 500, 1, 5, 'image/nam-linh-chi-01.jpg'),
(40, 'TÁO ĐỎ TÂN CƯƠNG', 11, 0, '<ol><li><strong>Táo đỏ Tân Cương</strong></li></ol><ul><li>Hoàng Yến luôn lựa chọn những quả táo chất lượng nhất từ vùng đất Nhược Khương và Hoà Điền của Trung Quốc, táo đỏ Tân Cương với vị thơm đặc trưng sẽ khiến quý khách rất ưng ý.</li><li>Công dụng: Táo đỏ Tân Cương của Hoàng Yến là một vị thuốc trong Đông Y với những tác dụng rất tốt như:</li><li>Cải thiện giấc ngủ và chức năng não</li><li>Giúp tốt cho hệ tim mạch</li><li>Phòng ngừa bệnh Alzheimer</li><li>Tăng cường cho hệ miễn dịch,ngăn ngừa ung thư</li><li>Giúp ăn ngon ngủ ngon hơn.</li><li>Hỗ trợ giảm cân, cải thiện hệ tiêu hoá</li><li>Hỗ trợ tăng sức khoẻ xương</li><li>Giúp kháng khuẩn, trị cảm mạo</li><li>Chữa viêm gan cấp tính, xơ gan</li><li>Chống dị ứng phát ban</li><li>Cách sử dụng:</li><li>Ăn trực tiếp</li><li>Khi kết hợp táo đỏ để chưng yến sẽ cho ra những hũ yến chưng thơm ngon, đẹp mắt</li><li>Nấu chè dưỡng nhan</li><li>Pha trà : Táo đỏ + hoa cúc chi sấy khô + kỷ tử + đường phèn giúp tách trà thơm, giúp an thần, ngủ ngon, tiêu hóa tốt</li><li>Táo đỏ ngâm rượu : 1 kg táo đỏ + 3 lít rượu, ngâm trong vòng 3 tháng sau đó lấy ra dung dần</li><li>Tần gà, tần chim: hương vị táo đỏ kết hợp sẽ cho món ăn thơm ngon</li><li>Làm mứt: Đường đen nấu chảy ra trộn với táo đỏ, cho vào nồi xào với lửa nhỏ liu riu cho đến khi táo mềm và ngấm đường là được.</li><li>Táo đỏ Tân Cương của Hoàng Yến với hương vị dễ ăn và dùng được cho mọi đối tượng.</li></ul><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: <a href=\"https://www.facebook.com/hoangyensanest/\"><strong>Yến sào Hoàng Yến</strong></a>&nbsp;</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 200000.00, 180, 1, 5, 'image/tao-do.jpg'),
(41, 'BỔ GAN HÀN QUỐC', 11, 0, '<p><strong>Bổ gan Comfortable Morning Hovenia</strong></p><ul><li>Xuất xứ: Hàn Quốc</li><li>Được chiết xuất từ cây Hovenia, một loại thảo dược quý được trồng chủ yếu ở Hàn Quốc, Nhật Bản, và đã được các nhà khoa học chứng minh có tác dụng rất tích cực trong việc:</li><li>Tăng cường các chức năng gan, giúp thải trừ các độc tố có hại, hỗ trợ gan khoẻ mạnh và có khả năng phục hồi tốt cho các trường hợp nhiễm bệnh gan.</li><li>&nbsp;Đồng thời hạt Hovenia là một vị thuốc bổ có tác dụng bồi bổ cơ thể, thanh nhiệt giải độc, giải trừ các độc tố có hại trong rượu và làm mất các triệu chứng sau khi uống rượu như buồn nôn, đau đầu, ợ nóng</li><li>Nhuận tràng, làm hết mụn nhọt, trứng cá, làm đẹp da phụ nữ bằng cách thải trừ chất bã nhờn, chống lão hoá sớm ở nam do lạm dụng hoặc thường xuyên dùng đồ uống có cồn</li><li>Tác dụng giải rượu của Hovenia giúp cơ thể không có cảm giác mệt mỏi, khô mỏi khớp xương sau khi ngủ dậy.</li><li>Cách sử dụng: Ngày uống 1-2 lần, mỗi lần 1 gói, lắc đều trước khi sử dụng. Bảo quản nơi khô ráo thoáng mát. Tốt nhất nên bảo quản trong ngăn mát tủ lạnh. Nếu dùng để giải rượu thì uống ngay sau khi uống rượu.</li><li>Hạn sử dụng in trên bao bì</li><li>Hộp quà sang trọng lịch sự, phù hợp cả đi biếu tặng. Hoàng Yến cung cấp số lượng lớn.</li></ul><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p><p>Website: yensaohaiphong.xyz</p><p>Fanpage: Yến sào Hoàng Yến</p><p>Facebook:&nbsp; https://www.facebook.com/hoangyensanest/</p><p>Email: <a href=\"mailto:maihuongqh@gmail.com\"><strong>maihuongqh@gmail.com</strong></a></p>', 700000.00, 50, 1, 6, 'image/bo-gan.jpg');
INSERT INTO `product` (`id`, `name`, `typeProductId`, `view`, `description`, `retailPrice`, `max`, `min`, `vendorId`, `image`) VALUES
(42, 'TRÀ HOA CÚC', 11, 0, '<p><strong>Tên sản phẩm:</strong> Trà Hoa Cúc</p><p>• <strong>NHÀ YẾN VIỆT</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 02 cơ sở:<br>• Cơ sở 1: Số 187 Trần Thành Ngọ, quận Kiến An, Hải Phòng<br>• Cơ sở 2: Số 120 Lương Khánh Thiện, Ngô Quyền, Hải Phòng</p><p>• Cơ sở 3: Số 133 Nguyễn Bình, Đồng Quốc Bình (phố Văn Cao rẽ vào), quận Ngô Quyền, Hải Phòng</p><p><br>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Nha Trang - Hải Phòng</strong></p>', 180000.00, 80, 3, 1, 'image/tra-hoa-cuc01.jpg'),
(43, 'NHUỴ HOA NGHỆ TÂY - SAFFRON BAHRAMAN', 11, 0, '<p><strong>Tên sản phẩm: Saffron Bahraman Trung Cấp</strong></p><p><strong>Saffron Bahraman</strong>&nbsp;thuộc dòng có kiểm soát, là những sản phẩm Saffron mà trong quá trình trồng trọt, canh tác và thu hoạch có sử dụng một lượng phân bón trong mức cho phép. Chính vì thế, Saffron Bahraman vẫn đảm bảo chất lượng, giữ được mùi hương và giá trị dinh dưỡng cao mà vẫn an toàn đối với sức khỏe người dùng.</p><p><strong>Xuất xứ: Iran</strong></p><p><strong>Chứng nhận:&nbsp;</strong>ISO 3632</p><p><strong>Quy cách đóng gói:</strong>&nbsp;1 lọ 1gr</p><p><strong>Bảo quản:&nbsp;</strong>để ở nơi khô, thoáng, mát. Tránh ánh nắng trực tiếp chiếu vào.</p><p><strong>Hạn sử dụng:</strong>&nbsp;HSD được in trên vỏ hộp</p><p><strong>1. Sử Dụng Uống Saffron Hằng Ngày</strong></p><p>Vừa đơn giản vừa mang lại hiệu quả cao, các chất dinh dưỡng có trong saffron đóng vai trò quan trọng như một chất chống lão hóa làm đẹp da từ bên trong.<br>&nbsp;</p><ul><li>Cách sử dụng nhụy hoa nghệ tây để uống rất đơn giản, bạn chỉ cần thả 3-5 sợi saffron vào nước sôi hoặc cốc sữa ấm trong vòng 3-5 phút để saffron ngấm màu ra nước, bạn cũng có thể bỏ thêm 1 thìa cà phê mật ong vào rồi uống.</li><li>Đối với phụ nữ đang mang thai saffron đem lại rất nhiều lợi ích cho sức khỏe cả mẹ và bé nhất là sau khi thai kì được 5 tháng, tuy nhiên trước khi sử dụng saffron nên hỏi ý kiến của bác sĩ để có lời khuyên chính xác nhất, tránh các tác dụng phụ.</li><li>Uống saffron không chỉ đơn giản mà còn giúp khai thác tối ưu tác dụng của nó, giúp saffron phát huy công dụng hiệu quả nhất nên được rất nhiều người sử dụng.</li></ul><p>• <a href=\"http://www.royalsanest.com/\"><strong>YẾN SÀO HOÀNG YẾN</strong></a> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Website: yensaohaiphong.xyz</p><p>Hotline: 0904224563</p>', 350000.00, 80, 1, 8, 'image/nhuy-hoa-nghe-tay.jpg'),
(44, 'YẾN TINH CHẾ VIP 1', 3, 0, '<p><strong>Tên sản phẩm:</strong> Yến tinh chế VIP 1</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website: yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 6500000.00, 40, 1, 3, 'image/yen-tinh-che-vip-1.jpg'),
(45, 'YẾN TINH CHẾ VIP 2', 3, 0, '<p><strong>Tên sản phẩm:</strong> Yến tinh chế VIP 2</p><p>• <strong>YẾN SÀO HOÀNG YẾN</strong> luôn tư vấn nhiệt tình miễn phí 24/24 qua điện thoại /Zalo/ website theo số 0904224563 .<br>• Để được phục vụ giao hàng nhanh nhất, quý khách hãy gọi trực tiếp đến số 0904224563 , chúng tôi miễn phí giao hàng khu vực Hải Phòng<br>• Mua hàng trực tiếp tại 03 cơ sở:<br>Cơ sở 1: số 24. Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: số 133 Nguyễn Bình, Ngô Quyền, Hải Phòng.</p><p>Cơ sở 3: số 187 Trần Thành Ngọ, Kiến An , Hải Phòng.</p><p>Hotline: 0904224563</p><p>Website:yensaohaiphong.xyz</p><p>• Đặt hàng qua website: Quý Khách vào trang chủ chọn sản phẩm cần mua và thêm vào giỏ hàng. Sau đó điền đầy đủ thông tin nhận hàng. Chúng tôi sẽ gọi điện và xác nhận đơn hàng của Quý Khách.<br>• Gửi tin nhắn qua website: Quý khách gửi tin nhắn, để lại số điện thoại chúng tôi sẽ tư vấn miễn phí và giúp quý khách lựa chọn được loại yến sào phù hợp với mục đích của mình.<br>• Gửi tin nhắn/zalo 0904224563 cho chúng tôi<br>• Gửi tin nhắn qua Fanpage: <strong>Yến Sào Hoàng Yến - Hải Phòng</strong></p>', 6800000.00, 58, 1, 5, 'image/yen-tinh-che-vip2.jpg'),
(46, 'HỒNG YẾN TINH CHẾ HẢO HẠNG', 5, 0, '<p><strong>Hồng Yến tinh chế Thượng hạng</strong></p><p>Với sản lượng chỉ chiếm 1% tổng sản lượng yến sào thì Hồng Yến là mặt hàng quý giá và là món quà biếu mang giá trị cao cấp. Hồng yến của Hoàng Yến có màu cam hồng nhẹ bởi màu hoàn toàn tự nhiên. Hoàng yến cam kết không ủ màu cho Hồng yến, vì thế giữ được chất lượng cao nhất của dòng yến quý giá này.</p><p>&nbsp;</p><p>Hồng yến tinh chế Thượng hạng của Hoàng yến có những vi chất rất quý giá, chứa hàm lượng Protein cao, nhiều loại acid amin, khoáng chất cần thiết mà con người không tự tổng hợp được, và các thành phần hữu ích như: glycol, aspartic acid, serine, tyrosine, phenylalanine, valine, arginine, leucine….</p><p>&nbsp;</p><p>Khi mua Hồng yến tinh chế thượng hạng tại Hoàng Yến, quý khách được tặng những món quà tặng như : thố/hũ thủy tinh chưng yến, táo đỏ, hạt chia, kỷ tử, long nhãn, đường phèn…</p><p>Hoàng Yến tự hào là nơi cung cấp những tổ Hồng yến tinh chế Thượng hạng cao cấp và uy tín,&nbsp; là địa chỉ tin cậy của rất nhiều khách hàng.</p><p>&nbsp;</p><p><i><strong>Hoàng Yến – Yến tiến Vua – Thương hiệu tự hào của người Việt!</strong></i></p><p><i><strong>Nơi cung cấp Yến sào – Yến hũ đóng chai – Đông trùng hạ thảo chất lượng nhất</strong></i></p><p>Hệ thống cửa hàng YẾN SÀO HOÀNG YẾN</p><p>Cơ sở 1: Số 24, Đà Nẵng, Ngô Quyền, Hải Phòng</p><p>Cơ sở 2: Số 133 Nguyễn Bình, P. Đồng Quốc Bình, Ngô Quyền, Hải Phòng</p><p>Cơ sở 3: Số 187, Trần Thành Ngọ, quận Kiến An, Hải Phòng</p><p>Hotline: 0904.224.563</p>', 5000000.00, 520, 10, 3, 'image/1.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_detail`
--

CREATE TABLE `product_detail` (
  `id` int(11) NOT NULL,
  `fromDate` datetime NOT NULL,
  `toDate` datetime NOT NULL,
  `importPrice` decimal(18,2) NOT NULL,
  `taxAmount` decimal(18,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `quantitySold` int(11) NOT NULL,
  `quantityCurrent` int(11) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `inwardSlipVoucherId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_detail`
--

INSERT INTO `product_detail` (`id`, `fromDate`, `toDate`, `importPrice`, `taxAmount`, `quantity`, `quantitySold`, `quantityCurrent`, `warehouseId`, `productId`, `inwardSlipVoucherId`) VALUES
(18, '2021-05-29 16:10:19', '2021-05-29 16:10:19', 1500000.00, 16000.00, 3, 0, 3, 5, 11, 19),
(19, '2021-05-29 16:10:21', '2021-05-29 16:10:21', 1500000.00, 0.00, 1, 0, 10, 5, 10, 19),
(20, '2021-05-29 16:12:20', '2021-05-29 16:12:20', 3500000.00, 0.00, 3, 0, 3, 5, 12, 20),
(21, '2021-05-29 16:13:22', '2021-05-29 16:13:22', 20000.00, 15.00, 4, 0, 4, 5, 17, 21),
(22, '2021-05-29 16:13:55', '2021-05-29 16:13:55', 7000000.00, 15.00, 3, 0, 3, 5, 16, 22),
(23, '2021-06-01 17:28:50', '2021-06-01 17:28:50', 15000.00, 15000.00, 1, 0, 1, 7, 4, 23),
(24, '2021-06-01 17:30:32', '2021-06-01 17:30:32', 1111111.00, 62000.00, 1, 0, 1, 5, 13, 23),
(25, '2021-06-01 17:30:37', '2021-06-01 17:30:37', 20000.00, 60000.00, 1, 0, 1, 5, 17, 23),
(30, '2021-06-02 06:45:13', '2021-06-02 06:45:13', 7000000.00, 0.00, 1, 0, 1, 5, 16, 26),
(31, '2021-06-02 06:45:14', '2021-06-02 06:45:14', 3500000.00, 0.00, 1, 0, 1, 5, 12, 26),
(32, '2021-06-08 19:07:23', '2021-07-10 00:00:00', 2000000.00, 20000.00, 10, 0, 10, 5, 1, 27),
(33, '2021-06-08 20:24:45', '2021-06-08 20:24:45', 7500000.00, 0.00, 50, 0, 50, 7, 32, 28),
(34, '2021-06-08 20:25:01', '2021-06-08 20:25:01', 7000000.00, 0.00, 16, 0, 16, 5, 16, 28),
(35, '2021-06-08 20:25:11', '2021-06-08 20:25:11', 850000.00, 0.00, 50, 0, 25, 6, 30, 28),
(36, '2021-06-08 20:34:03', '2021-06-08 20:34:03', 3500000.00, 0.00, 10, 0, 10, 5, 23, 29),
(37, '2021-06-08 20:34:12', '2021-06-08 20:34:12', 6500000.00, 0.00, 10, 0, 10, 6, 26, 29),
(38, '2021-06-08 20:37:43', '2021-06-08 20:37:43', 1500000.00, 0.00, 5, 0, 5, 5, 11, 30),
(39, '2021-06-08 20:38:05', '2021-06-08 20:38:05', 4000000.00, 0.00, 16, 0, 16, 7, 20, 30),
(40, '2021-06-08 20:41:09', '2021-06-08 20:41:09', 700000.00, 0.00, 2, 0, 2, 5, 41, 31),
(41, '2021-06-08 20:41:20', '2021-06-08 20:41:20', 500000.00, 0.00, 6, 0, 6, 5, 29, 31),
(42, '2021-06-08 20:42:43', '2021-06-08 20:42:43', 600000.00, 0.00, 10, 0, 10, 7, 41, 32),
(43, '2021-06-08 20:42:45', '2021-06-08 20:42:45', 300000.00, 0.00, 16, 0, 16, 5, 29, 32),
(44, '2021-06-08 20:42:47', '2021-06-08 20:42:47', 1000000.00, 0.00, 18, 0, 5, 6, 10, 32),
(45, '2021-03-09 00:00:00', '2023-06-09 00:00:00', 600000.00, 0.00, 50, 0, 50, 6, 36, 33),
(46, '2021-06-08 20:46:55', '2023-06-29 00:00:00', 180000.00, 0.00, 18, 0, 18, 5, 42, 33),
(47, '2021-06-08 20:50:22', '2024-06-09 00:00:00', 6500000.00, 0.00, 1, 0, 0, 5, 26, 34),
(48, '2021-06-07 00:00:00', '2023-06-09 00:00:00', 3500000.00, 0.00, 1, 0, 0, 5, 23, 34),
(49, '2021-04-07 00:00:00', '2025-11-07 00:00:00', 350000.00, 0.00, 1, 0, 0, 5, 43, 34),
(50, '2021-05-31 00:00:00', '2022-06-09 00:00:00', 2000000.00, 0.00, 1, 0, 1, 5, 33, 34),
(51, '2021-05-05 00:00:00', '2024-06-09 00:00:00', 1000000.00, 0.00, 1, 0, 0, 5, 37, 34),
(52, '2021-06-08 20:53:29', '2023-06-09 00:00:00', 700000.00, 0.00, 11, 0, 0, 5, 41, 35),
(53, '2021-06-08 20:53:31', '2022-06-09 00:00:00', 500000.00, 0.00, 8, 0, 8, 5, 29, 35),
(54, '2021-06-14 00:00:00', '2024-06-05 00:00:00', 1000000.00, 10000.00, 1, 0, 0, 7, 11, 36),
(55, '2021-06-09 01:31:36', '2021-06-09 01:31:36', 4000000.00, 0.00, 1, 0, 0, 6, 20, 36),
(56, '2021-06-09 01:31:38', '2021-06-09 01:31:38', 750000.00, 0.00, 1, 0, 0, 8, 17, 36),
(57, '2021-06-09 01:33:10', '2021-06-09 01:33:10', 1500000.00, 10000.00, 10, 0, 15, 6, 10, 37),
(58, '2021-06-09 01:33:12', '2021-06-09 01:33:12', 700000.00, 50000.00, 15, 0, 0, 8, 41, 37),
(59, '2021-05-28 00:42:15', '2021-05-28 00:42:15', 1500000.00, 150.00, 6, 0, 0, 7, 11, 15),
(60, '2021-05-29 09:04:20', '2021-05-29 09:04:20', 1500000.00, 0.00, 2, 0, 10, 5, 10, 15),
(61, '2021-06-14 08:54:23', '2021-06-14 08:54:23', 850000.00, 5000.00, 5, 0, 30, 5, 30, 38),
(62, '2021-06-14 10:02:17', '2021-05-14 00:00:00', 1500000.00, 10000.00, 4, 0, 0, 5, 11, 39);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `receipt_voucher`
--

CREATE TABLE `receipt_voucher` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `receiveDate` datetime NOT NULL,
  `employeeId` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `return_voucher`
--

CREATE TABLE `return_voucher` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `decription` varchar(200) NOT NULL,
  `employeeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `return_voucher_detail`
--

CREATE TABLE `return_voucher_detail` (
  `id` int(11) NOT NULL,
  `returnVoucherId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `decription` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shipping_voucher`
--

CREATE TABLE `shipping_voucher` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `shippingType` tinyint(4) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `shippingStatus` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `shippingNote` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `shipping_voucher`
--

INSERT INTO `shipping_voucher` (`id`, `orderId`, `shippingType`, `dateCreated`, `shippingStatus`, `employeeId`, `shippingNote`) VALUES
(20, 24, 3, '2021-06-16 18:13:06', 1, 8, ''),
(21, 38, 2, '2021-06-13 18:15:33', 0, 19, ''),
(22, 31, 1, '2021-06-13 19:28:45', 0, 8, ''),
(23, 42, 0, '2021-06-13 21:02:57', 0, 8, ''),
(24, 38, 4, '2021-06-14 00:04:20', 2, 19, ''),
(25, 43, 0, '2021-06-14 15:07:52', 0, 19, ''),
(26, 45, 0, '2021-06-14 15:20:29', 0, 19, ''),
(27, 45, 0, '2021-06-14 15:29:03', 0, 19, ''),
(28, 45, 0, '2021-06-14 15:29:40', 0, 19, ''),
(29, 45, 0, '2021-06-14 15:30:25', 0, 19, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shipping_voucher_progess`
--

CREATE TABLE `shipping_voucher_progess` (
  `id` int(11) NOT NULL,
  `shippingVoucherId` int(11) NOT NULL,
  `updateTime` datetime NOT NULL DEFAULT current_timestamp(),
  `currentStatus` tinyint(4) NOT NULL,
  `reason` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transporter`
--

CREATE TABLE `transporter` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `website` text NOT NULL,
  `address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `transporter`
--

INSERT INTO `transporter` (`id`, `name`, `phone`, `website`, `address`) VALUES
(5, 'Giao hàng tiết kiệm', '02020220020', 'giaohangtietkiem.com', 'HP'),
(7, 'Chuyển phát nhanh', '0223344556', 'giaohangnhanhchong', 'Hà Nam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `typeproduct`
--

CREATE TABLE `typeproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `typeproduct`
--

INSERT INTO `typeproduct` (`id`, `name`, `path`) VALUES
(2, 'ĐÔNG TRÙNG HẠ THẢO', 'hong-yen'),
(3, 'BẠCH YẾN', 'hong-bang'),
(5, 'HỒNG YẾN', 'dong-trung-ha-thao'),
(6, 'HỘP QUÀ', 'dong-trung-ha-thao'),
(7, 'YẾN CHƯNG SẴN', 'yen-chung'),
(8, 'TUYẾT YẾN', ''),
(11, 'Sản Phẩm Khác', 'San-pham-khac');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `name` varchar(50) NOT NULL,
  `avatar` text NOT NULL,
  `phone` varchar(12) NOT NULL,
  `dob` datetime NOT NULL,
  `address` text NOT NULL,
  `email` varchar(320) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `passport` varchar(13) NOT NULL,
  `passportDate` datetime NOT NULL,
  `passportPlace` varchar(255) NOT NULL,
  `warehouseId` int(11) NOT NULL,
  `isActivated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `dateCreated`, `name`, `avatar`, `phone`, `dob`, `address`, `email`, `gender`, `passport`, `passportDate`, `passportPlace`, `warehouseId`, `isActivated`) VALUES
(6, 'huongvt', 'Abc@123', '2021-05-23 14:41:05', 'Vũ Thị Hương', 'image/huong1.jpg', '0348712682', '2021-05-21 23:25:21', '97 Hào Khê, Hải Phòng', 'huong76591@gmail.com', 1, '123132331', '0000-00-00 00:00:00', 'ABC', 5, 1),
(7, ' hoangdv', 'Abc@123', '2021-05-18 14:44:12', 'Đặng Việt Hoàng', '', '03525525', '2021-05-23 14:44:12', 'Thủy Nguyên, Hải Phòng', 'abc@gmail.com', 1, '123', '0000-00-00 00:00:00', 'ABC', 5, 1),
(8, 'admin', 'Abc@123', '2021-05-23 15:06:44', 'Quản trị viên', 'image/beauty_20190419182448.jpg', '0258741362', '2021-05-26 08:06:44', 'HP', 'admin@gmail.com', 1, '123456789101', '2021-04-29 04:58:26', 'ABC', 5, 1),
(9, 'minhdang', 'Abc@123', '2021-05-01 00:00:00', 'Đặng Minh', 'image/glx.jpg', '0258258521', '1999-12-31 17:00:00', 'Thái Bình', 'gg@mail.com', 1, '033344112', '1999-12-31 17:00:00', 'TB', 6, 0),
(13, 'khachoang', 'Abc@123', '0000-00-00 00:00:00', 'Phạm Khắc Hoàng', 'image/cat.jpg', '01234567890', '2021-04-30 16:01:48', 'Hải Phòng', '020202@gmail.com', 1, '1234567891012', '2021-05-09 16:01:55', 'HP', 7, 1),
(16, 'xinchaobnhe', 'Abc@123', '0000-00-00 00:00:00', 'Thanh', 'image/25.jpg', '0223344559', '2021-05-05 02:17:12', 'Hà Nam', 'thanhzz@gmail.com', 1, '06646978211', '2021-05-03 02:17:30', 'Hà Nam', 8, 1),
(19, 'chaoban', '34', '2021-05-25 22:48:01', 'hihigido1', 'image/1.jpg', '0234567891', '2021-05-24 21:59:56', 'Ninh Bình', 'kudo@gmail.com', 1, '012345678910', '2021-05-05 21:59:56', 'Tây Ninh', 7, 1),
(22, 'huong', 'Abc@123', '0000-00-00 00:00:00', 'Vũ T Hương', 'image/huong1.jpg', '0348712682', '2021-05-20 16:28:49', '123 Ngô Quyền', 'Huong@gmail.com', 2, '121231234', '2021-05-28 16:29:04', 'Thái bÌnh', 8, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(320) NOT NULL,
  `taxCode` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `vendor`
--

INSERT INTO `vendor` (`id`, `name`, `phone`, `address`, `email`, `taxCode`) VALUES
(1, 'Hoàng', '0333444111', 'Viet Nam', 'hhhh@gg.com', '3033'),
(3, 'Samsung', '0333444111', 'Korea', 'samsung@gg.com', '3033'),
(5, 'Thanh Hải', '0343434212', 'Hà Nam', 'thanhhaizz98@gmail.com', '3032'),
(6, 'Aeon Mall', '0223331111', 'HP', 'eee@gmail.com', '020202'),
(8, 'Vũ Thị Hương', '0348712682', '123 Ngô Quyền', 'huong@gmail.com', '121212');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(200) NOT NULL,
  `capacity` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `warehouse`
--

INSERT INTO `warehouse` (`id`, `name`, `location`, `capacity`, `isDeleted`) VALUES
(5, 'Kiến An', '200 Trần Thành Ngọ', 25000, 0),
(6, 'Nguyễn Bình ', '31 Nguyễn Bình', 15000, 0),
(7, 'H01', 'Hải phòng', 20000, 0),
(8, 'Akho', 'Lê Chân', 17500, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `inventoryDelivery_voucher`
--
ALTER TABLE `inventoryDelivery_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouseFrom` (`warehouseFrom`),
  ADD KEY `warehouseTo` (`warehouseTo`);

--
-- Chỉ mục cho bảng `inventoryDelivery_voucher_detail`
--
ALTER TABLE `inventoryDelivery_voucher_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inventoryDeliveryVoucherId` (`inventoryDeliveryVoucherId`),
  ADD KEY `productId` (`productId`);

--
-- Chỉ mục cho bảng `inwardSlip_voucher`
--
ALTER TABLE `inwardSlip_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employeeId` (`employeeId`),
  ADD KEY `vendorId` (`vendorId`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customerID` (`customerId`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderID` (`orderId`),
  ADD KEY `productID` (`productId`);

--
-- Chỉ mục cho bảng `payment_voucher`
--
ALTER TABLE `payment_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paymentmethod_ibfk_2` (`shippingID`),
  ADD KEY `payment_voucher_ibfk_1` (`inwardSlipVoucherId`);

--
-- Chỉ mục cho bảng `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `permission_detail`
--
ALTER TABLE `permission_detail`
  ADD PRIMARY KEY (`userID`,`permissionID`),
  ADD KEY `permissionID` (`permissionID`);

--
-- Chỉ mục cho bảng `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryID` (`categoryId`),
  ADD KEY `userId` (`userId`);

--
-- Chỉ mục cho bảng `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendorId` (`vendorId`),
  ADD KEY `typeProductID` (`typeProductId`);

--
-- Chỉ mục cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productId`),
  ADD KEY `inwardSlipID` (`inwardSlipVoucherId`),
  ADD KEY `warehouseId` (`warehouseId`);

--
-- Chỉ mục cho bảng `receipt_voucher`
--
ALTER TABLE `receipt_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `receipt_voucher_ibfk_1` (`employeeId`),
  ADD KEY `receipt_voucher_ibfk_2` (`orderId`);

--
-- Chỉ mục cho bảng `return_voucher`
--
ALTER TABLE `return_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employeeID` (`employeeId`),
  ADD KEY `warehouseID` (`warehouseId`),
  ADD KEY `orderId` (`orderId`);

--
-- Chỉ mục cho bảng `return_voucher_detail`
--
ALTER TABLE `return_voucher_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productId`),
  ADD KEY `returnSlipID` (`returnVoucherId`);

--
-- Chỉ mục cho bảng `shipping_voucher`
--
ALTER TABLE `shipping_voucher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderID` (`orderId`),
  ADD KEY `employeeID` (`employeeId`);

--
-- Chỉ mục cho bảng `shipping_voucher_progess`
--
ALTER TABLE `shipping_voucher_progess`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `transporter`
--
ALTER TABLE `transporter`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `typeproduct`
--
ALTER TABLE `typeproduct`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouseId` (`warehouseId`);

--
-- Chỉ mục cho bảng `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT cho bảng `inventoryDelivery_voucher`
--
ALTER TABLE `inventoryDelivery_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT cho bảng `inventoryDelivery_voucher_detail`
--
ALTER TABLE `inventoryDelivery_voucher_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT cho bảng `inwardSlip_voucher`
--
ALTER TABLE `inwardSlip_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT cho bảng `payment_voucher`
--
ALTER TABLE `payment_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT cho bảng `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT cho bảng `receipt_voucher`
--
ALTER TABLE `receipt_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `return_voucher`
--
ALTER TABLE `return_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `return_voucher_detail`
--
ALTER TABLE `return_voucher_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `shipping_voucher`
--
ALTER TABLE `shipping_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `shipping_voucher_progess`
--
ALTER TABLE `shipping_voucher_progess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `transporter`
--
ALTER TABLE `transporter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `typeproduct`
--
ALTER TABLE `typeproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `inventoryDelivery_voucher`
--
ALTER TABLE `inventoryDelivery_voucher`
  ADD CONSTRAINT `inventoryDelivery_voucher_ibfk_1` FOREIGN KEY (`warehouseFrom`) REFERENCES `warehouse` (`id`),
  ADD CONSTRAINT `inventoryDelivery_voucher_ibfk_2` FOREIGN KEY (`warehouseTo`) REFERENCES `warehouse` (`id`);

--
-- Các ràng buộc cho bảng `inventoryDelivery_voucher_detail`
--
ALTER TABLE `inventoryDelivery_voucher_detail`
  ADD CONSTRAINT `inventoryDelivery_voucher_detail_ibfk_1` FOREIGN KEY (`inventoryDeliveryVoucherId`) REFERENCES `inventoryDelivery_voucher` (`id`),
  ADD CONSTRAINT `inventoryDelivery_voucher_detail_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `inwardSlip_voucher`
--
ALTER TABLE `inwardSlip_voucher`
  ADD CONSTRAINT `inwardSlip_voucher_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `inwardSlip_voucher_ibfk_2` FOREIGN KEY (`vendorId`) REFERENCES `vendor` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`);

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_detail_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `payment_voucher`
--
ALTER TABLE `payment_voucher`
  ADD CONSTRAINT `payment_voucher_ibfk_1` FOREIGN KEY (`inwardSlipVoucherId`) REFERENCES `inwardSlip_voucher` (`id`),
  ADD CONSTRAINT `paymentmethod_ibfk_2` FOREIGN KEY (`shippingID`) REFERENCES `shipping_voucher` (`id`);

--
-- Các ràng buộc cho bảng `permission_detail`
--
ALTER TABLE `permission_detail`
  ADD CONSTRAINT `permission_detail_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `permission_detail_ibfk_2` FOREIGN KEY (`permissionID`) REFERENCES `permission` (`id`);

--
-- Các ràng buộc cho bảng `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `post_category` (`id`),
  ADD CONSTRAINT `post_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`vendorId`) REFERENCES `vendor` (`id`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`typeProductId`) REFERENCES `typeproduct` (`id`);

--
-- Các ràng buộc cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  ADD CONSTRAINT `product_detail_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `product_detail_ibfk_2` FOREIGN KEY (`inwardSlipVoucherId`) REFERENCES `inwardSlip_voucher` (`id`),
  ADD CONSTRAINT `product_detail_ibfk_3` FOREIGN KEY (`warehouseId`) REFERENCES `warehouse` (`id`);

--
-- Các ràng buộc cho bảng `receipt_voucher`
--
ALTER TABLE `receipt_voucher`
  ADD CONSTRAINT `receipt_voucher_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `receipt_voucher_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `return_voucher`
--
ALTER TABLE `return_voucher`
  ADD CONSTRAINT `return_voucher_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `return_voucher_ibfk_3` FOREIGN KEY (`warehouseId`) REFERENCES `warehouse` (`id`),
  ADD CONSTRAINT `return_voucher_ibfk_5` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `return_voucher_detail`
--
ALTER TABLE `return_voucher_detail`
  ADD CONSTRAINT `return_voucher_detail_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `return_voucher_detail_ibfk_2` FOREIGN KEY (`returnVoucherId`) REFERENCES `return_voucher` (`id`);

--
-- Các ràng buộc cho bảng `shipping_voucher`
--
ALTER TABLE `shipping_voucher`
  ADD CONSTRAINT `shipping_voucher_ibfk_3` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `shipping_voucher_ibfk_4` FOREIGN KEY (`employeeId`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`warehouseId`) REFERENCES `warehouse` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
